/*eslint-disable no-undef, no-restricted-globals*/
/*
 * Avaya Inc. Proprietary (Restricted)
 * Solely for authorized persons having a need to know
 * pursuant to company instructions.
 * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
 * The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 */
/**
 * Description
 */
 self.AvayaClientServicesWorker = {};
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  *
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @class
    * @define AvayaClientServicesWorker.BlocksComparator
    * @memberOf AvayaClientServicesWorker
    */
   function BlocksComparator() {}
 
   BlocksComparator.prototype =
     /** @lends AvayaClientServicesWorker.BlocksComparator.prototype */
     {
       /**
        *
        * @param previousBlocks
        * @param blockMatrix
        * @param blockWidth
        * @param blockHeight
        * @param losslessColorLimit
        * @param bufferPreparingFunction
        * @returns {Array}
        */
       compareBlocks: function(
         previousBlocks,
         blockMatrix,
         blockWidth,
         blockHeight,
         losslessColorLimit,
         bufferPreparingFunction
       ) {
         var changedBlocks = [];
         var columnsLength = blockMatrix.length;
         for (var column = 0; column < columnsLength; column++) {
           previousBlocks[column] = previousBlocks[column] || [];
           var rowsLength = blockMatrix[column].length;
           for (var row = 0; row < rowsLength; row++) {
             this._compareBlock(
               previousBlocks,
               blockMatrix[column][row],
               changedBlocks,
               row,
               column,
               blockWidth,
               blockHeight,
               losslessColorLimit,
               bufferPreparingFunction
             );
           }
         }
 
         return changedBlocks;
       },
       /**
        *
        * @param previousBlocks
        * @param rowBlocks
        * @param changedBlocks
        * @param row
        * @param column
        * @param blockWidth
        * @param blockHeight
        * @param losslessColorLimit
        * @param bufferPreparingFunction
        * @private
        */
       _compareBlock: function(
         previousBlocks,
         rowBlocks,
         changedBlocks,
         row,
         column,
         blockWidth,
         blockHeight,
         losslessColorLimit,
         bufferPreparingFunction
       ) {
         var base64Array = '';
         var len = rowBlocks.data.length;
         for (var i = 0; i < len; i++) {
           base64Array += btoa(rowBlocks.data[i]);
         }
         var previousBlock = previousBlocks[column][row];
         if (!previousBlock || base64Array !== previousBlock.base64) {
           var block = {
             _width: rowBlocks.width,
             _height: rowBlocks.data.length,
             _x: column * blockWidth,
             _y: row * blockHeight,
             _row: row,
             _column: column,
             _blocks: [],
             _data: rowBlocks.data
           };
           block._type = this._determineType(
             block._data,
             losslessColorLimit,
             bufferPreparingFunction
           );
           changedBlocks.push(block);
           previousBlocks[column][row] = {
             base64: base64Array
           };
         }
       },
       /**
        *
        * @param arr
        * @param losslessColorLimit
        * @param bufferPreparingFunction
        * @returns {number}
        * @private
        */
       _determineType: function(
         arr,
         losslessColorLimit,
         bufferPreparingFunction
       ) {
         var stats = {},
           buffer32,
           i,
           count = 0,
           len;
 
         var le2 = arr.length;
         for (var j = 0; j < le2; j++) {
           buffer32 = bufferPreparingFunction(arr[j]);
           len = buffer32.length;
           for (i = 0; i < len; i++) {
             var key = '' + (buffer32[i] & 0xffffff);
             if (!stats[key]) {
               stats[key] = 0;
               count++;
             }
             if (count > losslessColorLimit) {
               return 0;
             }
             stats[key]++;
           }
         }
 
         return 1;
       }
     };
 
   AvayaClientServicesWorker.BlocksComparator = BlocksComparator;
 })(self.AvayaClientServicesWorker);
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  *
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @class
    * @define AvayaClientServicesWorker.BlocksGenerator
    * @memberOf AvayaClientServicesWorker
    */
   function BlocksGenerator() {}
 
   BlocksGenerator.prototype =
     /** @lends AvayaClientServicesWorker.BlocksGenerator.prototype */
     {
       /**
        * @public
        * @function
        * @param blockWidth
        * @param blockHeight
        * @param currentScreen
        * @param screenWidth
        * @param dataPreparingFunction
        * @returns {Array}
        */
       generateBlocks: function(
         blockWidth,
         blockHeight,
         currentScreen,
         screenWidth,
         dataPreparingFunction
       ) {
         var primitiveBlocks = [];
         var length = currentScreen.length;
         var pixelsPerWidth = screenWidth * 4;
         var pixelsPerBlockWidth = blockWidth * 4;
         for (var i = 0, row = 0; i < length; i += pixelsPerWidth, row++) {
           var rowIndex = Math.floor(row / blockHeight);
 
           for (var j = 0; j < pixelsPerWidth; j += pixelsPerBlockWidth) {
             var len = pixelsPerBlockWidth;
             if (j + len > pixelsPerWidth) {
               len = pixelsPerWidth - j;
             }
 
             var column = Math.floor(j / pixelsPerBlockWidth);
             if (!primitiveBlocks[column]) {
               primitiveBlocks[column] = [];
             }
 
             var block = primitiveBlocks[column][rowIndex] || {
               data: [],
               width: len / 4
             };
             block.data = dataPreparingFunction(
               block,
               currentScreen.slice(i + j, i + j + len)
             );
             primitiveBlocks[column][rowIndex] = block;
           }
         }
 
         return primitiveBlocks;
       }
     };
 
   AvayaClientServicesWorker.BlocksGenerator = BlocksGenerator;
 })(self.AvayaClientServicesWorker);
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  *
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @class
    * @define AvayaClientServicesWorker.BlocksMerger
    * @memberOf AvayaClientServicesWorker
    */
   function BlocksMerger() {}
 
   BlocksMerger.prototype =
     /** @lends AvayaClientServicesWorker.BlocksMerger.prototype */
     {
       /**
        *
        * @param changedBlocks
        * @param rowMergingFunction
        * @returns {Array|U[]|any|JQuery|*}
        */
       mergeBlocks: function(changedBlocks, rowMergingFunction) {
         changedBlocks = this._sortByRow(changedBlocks);
         changedBlocks = this._mergeByRow(changedBlocks, rowMergingFunction);
 
         changedBlocks = this._sortByColumn(changedBlocks);
         changedBlocks = this._mergeByColumn(changedBlocks);
 
         return changedBlocks.map(this._createMergedBlock);
       },
       /**
        *
        * @param blocks
        * @returns {Array.<T>|*}
        * @private
        */
       _sortByColumn: function(blocks) {
         return blocks.sort(function(firstBlock, secondBlock) {
           if (firstBlock._column === secondBlock._column) {
             return firstBlock._row - secondBlock._row;
           } else {
             return firstBlock._column - secondBlock._column;
           }
         });
       },
 
       /**
        *
        * @param blocks
        * @returns {Array.<T>|*}
        * @private
        */
       _sortByRow: function(blocks) {
         return blocks.sort(function(firstBlock, secondBlock) {
           if (firstBlock._row === secondBlock._row) {
             return firstBlock._column - secondBlock._column;
           } else {
             return firstBlock._row - secondBlock._row;
           }
         });
       },
 
       /**
        *
        * @param blockA
        * @param blockB
        * @returns {boolean}
        * @private
        */
       _compareBlocksByColumn: function(blockA, blockB) {
         return (
           blockA._x === blockB._x &&
           blockA._type === blockB._type &&
           blockA._width === blockB._width &&
           (blockA._y + blockA._height === blockB._y ||
             blockB._y + blockB._height === blockA._y)
         );
       },
 
       /**
        *
        * @param blockA
        * @param blockB
        * @returns {boolean}
        * @private
        */
       _compareBlocksByRow: function(blockA, blockB) {
         return (
           blockA._y === blockB._y &&
           blockA._type === blockB._type &&
           blockA._height === blockB._height &&
           (blockA._x + blockA._width === blockB._x ||
             blockB._x + blockB._width === blockA._x)
         );
       },
 
       /**
        *
        * @param changedBlocks
        * @param compareFnc
        * @param callbackFnc
        * @returns {Array}
        * @private
        */
       _merge: function(changedBlocks, compareFnc, callbackFnc) {
         var result = [];
         var mergedBlock = this._copyBlock(changedBlocks[0]);
 
         for (var index = 1; index < changedBlocks.length; index++) {
           if (compareFnc(mergedBlock, changedBlocks[index])) {
             if (changedBlocks[index]._blocks.length > 0) {
               this._copyBlocks(changedBlocks, index, mergedBlock);
             } else {
               mergedBlock._blocks.push(changedBlocks[index]);
             }
             callbackFnc(mergedBlock, changedBlocks[index]);
           } else {
             result.push(mergedBlock);
             mergedBlock = this._copyBlock(changedBlocks[index]);
           }
         }
         result.push(mergedBlock);
         return result;
       },
 
       /**
        *
        * @param changedBlocks
        * @returns {*|Array}
        * @private
        */
       _mergeByColumn: function(changedBlocks) {
         return this._merge(changedBlocks, this._compareBlocksByColumn, function(
           mergedBlock,
           changedBlock
         ) {
           mergedBlock._height += changedBlock._height;
           mergedBlock._data = mergedBlock._data.concat(changedBlock._data);
         });
       },
 
       /**
        *
        * @param changedBlocks
        * @param rowMergingFunction
        * @returns {*|Array}
        */
       _mergeByRow: function mergeByRow(changedBlocks, rowMergingFunction) {
         return this._merge(changedBlocks, this._compareBlocksByRow, function(
           mergedBlock,
           changedBlock
         ) {
           rowMergingFunction(mergedBlock, changedBlock);
           mergedBlock._width += changedBlock._width;
         });
       },
 
       /**
        *
        * @param changedBlocks
        * @param index
        * @param mergedBlock
        */
       _copyBlocks: function copyBlocks(changedBlocks, index, mergedBlock) {
         changedBlocks[index]._blocks.forEach(function(b) {
           mergedBlock._blocks.push(b);
         });
       },
 
       /**
        *
        * @param changedBlock
        * @returns {*}
        * @private
        */
       _copyBlock: function(changedBlock) {
         var mergedBlock;
         if (changedBlock._blocks.length === 0) {
           mergedBlock = {
             _x: changedBlock._x,
             _y: changedBlock._y,
             _row: changedBlock._row,
             _column: changedBlock._column,
             _width: changedBlock._width,
             _height: changedBlock._height,
             _type: changedBlock._type,
             _blocks: [],
             _data: changedBlock._data
           };
           mergedBlock._blocks.push(changedBlock);
         } else {
           mergedBlock = changedBlock;
         }
         return mergedBlock;
       },
 
       /**
        *
        * @param block
        * @returns {{_x: (number|*), _y: (number|*), _row: *, _column: *, _width: (*|number), _height: (number|*), _type: *, _imgData: *}}
        */
       _createMergedBlock: function createMergedBlock(block) {
         var pixelsArray = block._data.reduce(function(previous, current) {
           Array.prototype.push.apply(previous, current);
           return previous;
         }, []);
         var imgData = new ImageData(
           new Uint8ClampedArray(pixelsArray),
           block._width,
           block._height
         );
 
         return {
           _x: block._x,
           _y: block._y,
           _row: block._row,
           _column: block._column,
           _width: block._width,
           _height: block._height,
           _type: block._type,
           _imgData: imgData
         };
       }
     };
 
   AvayaClientServicesWorker.BlocksMerger = BlocksMerger;
 })(self.AvayaClientServicesWorker);
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2016 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  *
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @class
    * @define AvayaClientServicesWorker.DataManipulator
    * @memberOf AvayaClientServicesWorker
    */
   function DataManipulator(blocksGenerator, blocksComparator, blocksMerger) {
     this._blocksGenerator = blocksGenerator;
     this._blocksComparator = blocksComparator;
     this._blocksMerger = blocksMerger;
     this._config = undefined;
 
     this._generatingKeyFrame = true;
     this._previousWidth = -1;
     this._previousHeight = -1;
     this._generatedFrames = 0;
     this._previousScreen = undefined;
   }
 
   DataManipulator.prototype =
     /** @lends AvayaClientServicesWorker.DataManipulator.prototype **/
     {
       /**
        * @public
        * @function
        * @param {boolean} generateKeyFrame
        */
       setKeyFrameGeneration: function(generateKeyFrame) {
         this._generatingKeyFrame = generateKeyFrame;
       },
       /**
        *
        * @param currentScreen
        * @param config
        * @param width
        * @param height
        * @returns {{changedBlocks: *, meta: {}}}
        */
       generateChangedBlocks: function(currentScreen, config, width, height) {
         var meta = {};
         this._config = config;
 
         this._determineKeyFrameGeneration(width, height);
 
         var start = Date.now();
         meta.prepareScreenTime = Date.now() - start;
 
         if (this._generatingKeyFrame) {
           this._previousScreen = [];
           this._generatingKeyFrame = false;
           meta.isKeyFrame = true;
         } else {
           meta.isKeyFrame = false;
         }
 
         start = Date.now();
 
         var changedBlocks = [];
         var changedBlocksIndex = {};
 
         var pixels = new Uint32Array(currentScreen.buffer);
 
         if (this._previousScreen && this._previousScreen.length > 0) {
           for (var i = 0; i < pixels.length; i++) {
             if (this._previousScreen[i] !== pixels[i]) {
               var completeLines = ~~(i / width);
               var changedRow = ~~(completeLines / this._config.blockSize);
               var changedColumn = ~~(
                 (i - completeLines * width) /
                 this._config.blockSize
               );
               changedBlocksIndex[changedColumn] =
                 changedBlocksIndex[changedColumn] || [];
               if (!changedBlocksIndex[changedColumn].includes(changedRow)) {
                 changedBlocksIndex[changedColumn][
                   changedBlocksIndex[changedColumn].length
                 ] = changedRow;
               }
 
               i += this._config.blockSize - (i % this._config.blockSize);
             }
           }
         }
 
         var isEqual =
           this._previousScreen &&
           this._previousScreen.length > 0 &&
           Object.keys(changedBlocksIndex).length === 0;
         if (!isEqual) {
           this._previousScreen = pixels;
           changedBlocks = this._quickDetermineChangedBlocks(
             changedBlocksIndex,
             currentScreen,
             width
           );
         }
         meta.determineChangedBlocksTime = Date.now() - start;
 
         if (changedBlocks.length > 0) {
           start = Date.now();
           changedBlocks = this._blocksMerger.mergeBlocks(
             changedBlocks,
             this._mergeByRow.bind(this)
           );
           meta.mergeBlocksTime = Date.now() - start;
           this._generatedFrames++;
         }
 
         return {
           changedBlocks: changedBlocks,
           meta: meta
         };
       },
 
       /**
        *
        * @param width
        * @param height
        * @private
        */
       _determineKeyFrameGeneration: function(width, height) {
         if (this._generatedFrames === this._config.framesPerKeyFrame) {
           this._generatingKeyFrame = true;
           this._generatedFrames = 0;
         }
         if (width !== this._previousWidth || height !== this._previousHeight) {
           this._previousWidth = width;
           this._previousHeight = height;
           this._generatingKeyFrame = true;
         }
       },
 
       /**
        *
        * @param changedBlocksIndex
        * @param currentScreen
        * @param width
        * @returns {*}
        * @private
        */
       _quickDetermineChangedBlocks: function(
         changedBlocksIndex,
         currentScreen,
         width
       ) {
         var changedBlocks = [];
         var length = currentScreen.length;
         var pixelsPerWidth = width * 4;
         var pixelsPerBlockWidth = this._config.blockSize * 4;
 
         var updatePreviousBlocks = function(
           changedColumn,
           changedRow,
           primitiveBlock
         ) {
           var block = {
             _width: primitiveBlock.width,
             _height: primitiveBlock.data.length,
             _x: changedColumn * this._config.blockSize,
             _y: changedRow * this._config.blockSize,
             _row: changedRow,
             _column: changedColumn,
             _blocks: [],
             _data: primitiveBlock.data,
             _type: AvayaClientServicesWorker.ScreenSharingImageTypes.PNG
           };
 
           var differentColors = {},
             count = 0;
 
           var R, G, B;
           var colorInt;
           var i, j;
           var done = false;
 
           var borderPixels = 0;
           var wasRed = false;
           var wasBlack = false;
 
           // Rows of pixels
           for (i = 0; i < block._data.length && !done; i += 2) {
             // Columns of pixels
             var buffer32 = this._prepareBuffer(block._data[i]);
             for (j = 0; j < buffer32.length && !done; j++) {
               colorInt = buffer32[j] & 0xffffff;
 
               if (
                 count <= this._config.losslessColorLimit &&
                 !differentColors[colorInt]
               ) {
                 differentColors[colorInt] = true;
                 count++;
               }
 
               R = colorInt & 0x0000ff;
               G = (colorInt >> 8) & 0x0000ff;
               B = (colorInt >> 16) & 0x0000ff;
 
               // JSCSDK-3827
               if ((R > 65 && G < 35) || (R > 180 && G < 55)) {
                 wasRed = true;
               }
 
               if ((R < 130 && G < 130 && B < 130) || B > 180) {
                 wasBlack = true;
               }
 
               if (wasBlack && wasRed) {
                 borderPixels++;
                 wasRed = false;
                 wasBlack = false;
               }
 
               if (count >= this._config.losslessColorLimit) {
                 block._type =
                   AvayaClientServicesWorker.ScreenSharingImageTypes.JPEG;
               }
 
               // Optimized numbers from tests
               if (borderPixels > 16) {
                 block._type =
                   AvayaClientServicesWorker.ScreenSharingImageTypes.JPEG_RED_WITH_BLACK;
                 done = true;
                 break;
               }
             }
           }
 
           changedBlocks.push(block);
         }.bind(this);
 
         if (Object.keys(changedBlocksIndex).length !== 0) {
           var generateChangedBlock = function(changedColumn, changedRow) {
             var block = { data: [], width: 0 };
 
             for (
               var primitiveRowIndex = changedRow,
                 row = primitiveRowIndex * this._config.blockSize,
                 i = row * pixelsPerWidth;
               primitiveRowIndex === changedRow && i < length;
               i += pixelsPerWidth,
                 row++,
                 primitiveRowIndex = ~~(row / this._config.blockSize)
             ) {
               for (
                 var primitiveColumnIndex = changedColumn,
                   j = primitiveColumnIndex * pixelsPerBlockWidth;
                 primitiveColumnIndex === changedColumn && j < pixelsPerWidth;
                 j += pixelsPerBlockWidth,
                   primitiveColumnIndex = ~~(j / pixelsPerBlockWidth)
               ) {
                 var len = pixelsPerBlockWidth;
                 if (j + len > pixelsPerWidth) {
                   len = pixelsPerWidth - j;
                 }
                 block.width = len / 4;
                 block.data = this._prepareBlockData(
                   block,
                   currentScreen.slice(i + j, i + j + len)
                 );
               }
             }
 
             return block;
           }.bind(this);
 
           for (var changedColumn in changedBlocksIndex) {
             if (changedBlocksIndex.hasOwnProperty(changedColumn)) {
               for (
                 var iColumn = 0;
                 iColumn < changedBlocksIndex[changedColumn].length;
                 iColumn++
               ) {
                 var changedBlock = generateChangedBlock(
                   parseInt(changedColumn),
                   changedBlocksIndex[changedColumn][iColumn]
                 );
                 updatePreviousBlocks(
                   changedColumn,
                   changedBlocksIndex[changedColumn][iColumn],
                   changedBlock
                 );
               }
             }
           }
         } else {
           // In this case all blocks are changed. No need to compare
           var primitiveBlocks = [];
           for (var i = 0, row = 0; i < length; i += pixelsPerWidth, row++) {
             var rowIndex = ~~(row / this._config.blockSize);
             for (var j = 0; j < pixelsPerWidth; j += pixelsPerBlockWidth) {
               var len = pixelsPerBlockWidth;
               if (j + len > pixelsPerWidth) {
                 len = pixelsPerWidth - j;
               }
               var columnIndex = ~~(j / pixelsPerBlockWidth);
               if (!primitiveBlocks[columnIndex]) {
                 primitiveBlocks[columnIndex] = [];
               }
               var block = primitiveBlocks[columnIndex][rowIndex] || {
                 data: [],
                 width: len / 4
               };
               block.data = this._prepareBlockData(
                 block,
                 currentScreen.slice(i + j, i + j + len)
               );
               primitiveBlocks[columnIndex][rowIndex] = block;
             }
           }
 
           for (
             var primitiveColumn = 0;
             primitiveColumn < primitiveBlocks.length;
             primitiveColumn++
           ) {
             var rowsLength = primitiveBlocks[primitiveColumn].length;
             for (
               var primitiveRow = 0;
               primitiveRow < rowsLength;
               primitiveRow++
             ) {
               updatePreviousBlocks(
                 primitiveColumn,
                 primitiveRow,
                 primitiveBlocks[primitiveColumn][primitiveRow]
               );
             }
           }
         }
 
         return changedBlocks;
       },
 
       _prepareScreen: function(currentScreen) {
         return currentScreen;
       },
 
       _prepareBlockData: function(block, data) {
         block.data = block.data.concat(data);
         return block.data;
       },
 
       _prepareBuffer: function(data) {
         return new Uint32Array(data.buffer);
       },
 
       _mergeByRow: function(mergedBlock, changedBlock) {
         var times = Math.ceil(mergedBlock._width / this._config.blockSize);
         for (var i = 0; i < changedBlock._data.length; i++) {
           Array.prototype.splice.apply(
             mergedBlock._data,
             [times + (times + 1) * i, 0].concat(changedBlock._data[i])
           );
         }
       }
     };
 
   AvayaClientServicesWorker.DataManipulator = DataManipulator;
 })(self.AvayaClientServicesWorker);
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  *
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @define AvayaClientServicesWorker.ScreenCapturerWorkerEvents
    * @enum {string}
    * @memberOf AvayaClientServicesWorker
    */
   var ScreenCapturerWorkerEvents = {
     INITIALIZE: 'initialize',
     CHANGED_BLOCKS: 'changedBlocksMessage',
     SEND_IMAGE_DATA: 'wholeImgData',
     WORKER_STARTED: 'workerStarted'
   };
 
   AvayaClientServicesWorker.ScreenCapturerWorkerEvents = ScreenCapturerWorkerEvents;
 })(self.AvayaClientServicesWorker);
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
   /**
    * @define ScreenSharingImageTypes.ScreenSharingImageTypes
    * @enum {number}
    * @memberOf AvayaClientServices.Providers.WCS
    */
   var ScreenSharingImageTypes = {
     JPEG: 0,
     PNG: 1,
     JPEG_RED_WITH_BLACK: 2
   };
   AvayaClientServicesWorker.ScreenSharingImageTypes = ScreenSharingImageTypes;
 })(self.AvayaClientServicesWorker);
 
 /*
  * Avaya Inc. Proprietary (Restricted)
  * Solely for authorized persons having a need to know
  * pursuant to company instructions.
  * Copyright 2006-2016 Avaya Inc. All Rights Reserved.
  * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
  * The copyright notice above does not evidence any actual or
  * intended publication of such source code.
  */
 /**
  * Description
  */
 (function(AvayaClientServicesWorker) {
   'use strict';
 
   var dataManipulator;
   var config;
 
   self.onmessage = function(m) {
     if (
       m.data.type ===
       AvayaClientServicesWorker.ScreenCapturerWorkerEvents.INITIALIZE
     ) {
       var blocksGenerator = new AvayaClientServicesWorker.BlocksGenerator();
       var blocksComparator = new AvayaClientServicesWorker.BlocksComparator();
       var blocksMerger = new AvayaClientServicesWorker.BlocksMerger();
       dataManipulator = new AvayaClientServicesWorker.DataManipulator(
         blocksGenerator,
         blocksComparator,
         blocksMerger
       );
 
       config = m.data.config;
       self.postMessage({
         type:
           AvayaClientServicesWorker.ScreenCapturerWorkerEvents.WORKER_STARTED
       });
     } else if (
       m.data.type ===
       AvayaClientServicesWorker.ScreenCapturerWorkerEvents.SEND_IMAGE_DATA
     ) {
       var start = Date.now();
 
       if (m.data.forceKeyFrame) {
         dataManipulator.setKeyFrameGeneration(true);
       }
 
       var result = dataManipulator.generateChangedBlocks(
         m.data.imgData,
         config,
         m.data.width,
         m.data.height
       );
       var transferArray = result.changedBlocks.map(function(r) {
         return r._imgData.data.buffer;
       });
       var metaInfo = {
         processingTime: Date.now() - start,
         processedBlocks: result.changedBlocks.length,
         keyFrame: result.meta.isKeyFrame,
         meta: result.meta,
         generatedPNGs: result.changedBlocks.reduce(function(previous, current) {
           if (
             current._type ===
             AvayaClientServicesWorker.ScreenSharingImageTypes.PNG
           ) {
             previous++;
           }
           return previous;
         }, 0)
       };
 
       self.postMessage(
         {
           type:
             AvayaClientServicesWorker.ScreenCapturerWorkerEvents.CHANGED_BLOCKS,
           data: result.changedBlocks,
           meta: metaInfo
         },
         transferArray
       );
     }
   };
 })(self.AvayaClientServicesWorker);
 