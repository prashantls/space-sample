/*
 * Avaya Inc. Proprietary (Restricted)
 * Solely for authorized persons having a need to know
 * pursuant to company instructions.
 * Copyright 2006-2015 Avaya Inc. All Rights Reserved.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF Avaya Inc.
 * The copyright notice above does not evidence any actual or
 * intended publication of such source code.
 */

import axios from 'axios';
import * as io from "socket.io-client";
import saveFile from 'file-saver';
import moment from 'moment'
import { Base64, getGUID } from '../utils'
export default class SampleAvayaSpacesSDK {
    constructor(props) {
        this.token = process.env.REACT_APP_TOKEN || '';
        this.socketURL = `wss://${localStorage.getItem("websocket") || 'spacesapis-socket.avayacloud'}.com/chat`;
        this.spaceId = "";
        this.conferenceCall = null;
        this.user = null;
        this.client = null
        this.mpaasToken = null
        this.micDevices = [];
        this.cameraDevices = [];

        this.speakerDevices = [];
        this.socketConnection = undefined;
        this.collaboration = undefined;
        this.collaborations = undefined;
        this.onScreenShare = false;
        this.audioMuted = true;
        this.videoMuted = true;
        this.localStream = null;
        this.recording = false;
        this.recordingId = "";
        this.meetingId = "";
        this.contentSharingRenderer = null;
        this.tokenType = 'jwt';
        this.remoteStream = null;
        this.sessionId = '';
        this.inviteId = '';
        this.EventEmitter = {
            events: {},
            dispatch: function (event, data) {
                if (!this.events[event]) return
                this.events[event].forEach(callback => callback(data))
            },
            subscribe: function (event, callback) {
                if (!this.events[event]) this.events[event] = []
                this.events[event].push(callback)
            }
        }
        this.sender = undefined;
        this.spacePosts = [];
        this.spaceTasks = [];
        this.spaceMeetings = [];
        this.spaceMembers = {};
        this.peopleWithDM = {}
        this.spaceTitle = ''
        this.anonAuthChosen = false
        this.isBase64 = str => {
            return Base64.isBase64(str);
        };
        this.base64Decode = str => {
            return Base64.decode(str);
        };

        this.previousSpace = ""
        this.mySpaces = {}
        this.colleaguesAndDMList = {}
        this.spacePinRequired = false
        this.authMethodChosen = ""
        this.cameraOptions = []
        this.micOptions = []
        this.speakersOptions = []
        this.env = process.env.REACT_APP_ENVIRONMENT
        this.userRaisedHand = ""	
        this.call = null	
        this.userSettings = ""	
        this.spaceContent = []	
        this.meetingData = null
        this.showModalPostEdit = false
        this.showModalTaskEdit = false
    }


    
    setEnv = (env) => {
        console.log('%c setEnv()', 'background: #0000ff; color: #fff');
        if (env !== undefined)  this.env = env
        if (this?.env === 'staging') {
            localStorage.setItem('spaces_app', 'loganstaging.esna')
            localStorage.setItem('accounts_app', 'onesnastaging.esna')
            localStorage.setItem('spaces_apis', 'loganstagingapis.esna')
            localStorage.setItem('websocket', 'loganstagingapis-socket.esna')
        }
        else if (this?.env === 'production') {
            localStorage.setItem('spaces_app', 'spaces.avayacloud')
            localStorage.setItem('accounts_app', 'accounts.avayacloud')
            localStorage.setItem('spaces_apis', 'spacesapis.avayacloud')
            localStorage.setItem('websocket', 'spacesapis-socket.avayacloud')
        }
    }


    getAnonSpacesToken = (anonUsername) => {
        console.log('%c getAnonSpacesToken()', 'background: #0000ff; color: #fff');
        // Get Spaces token for anonymous user
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                url: `https://${localStorage.getItem('spaces_apis')}.com/api/anonymous/auth`,
                data: {
                    "displayname": anonUsername,
                    "username": anonUsername
                },
                headers: { 'Content-Type': 'application/json' }
            }).then(res => {
                resolve(res.data.token)
            }).catch(err => {
                console.log(err)
                reject(err);
                this.EventEmitter.dispatch('error', { message: 'Could not retrieve anonymous token', data: JSON.stringify(err, null, " ") })
            })
        })
    }

    getUserInfo = () => {
        console.log('%c getUserInfo() ', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then((res) => {
            this.me = res.data
            this.EventEmitter.dispatch('state', { data: { me: this.me } })
            this.getPeopleWithDMList()
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve current user data', data: JSON.stringify(err, null, " ") })
        )
    }

    getPeopleWithDMList = () => {
        console.log('%c getPeopleWithDMList() ', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/people/withdm`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then(res => {
            // Coverted to object to avoid repeating members
            res.data.data.map(item => this.peopleWithDM = { ...this.peopleWithDM, [item._id]: item })
            this.EventEmitter.dispatch('state', { data: { peopleWithDM: this.peopleWithDM } })
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve people with DM list', data: JSON.stringify(err, null, " ") }))
    }

    getSpaceMemberList = () => {
        console.log('%c getSpaceMemberList() ', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/members`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then(res => {
            res.data.data.map(item => this.spaceMembers = { ...this.spaceMembers, [item.member]: item })
            this.EventEmitter.dispatch('state', { data: { spaceMembers: this.spaceMembers } })
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve space member list', data: JSON.stringify(err, null, " ") }))
    }

    redirectToAvayaCloudIdentity = (grant_type) => {
        console.log('%c redirectToAvayaCloudIdentity() ', 'background: #0000ff; color: #fff');
        return window.location = `https://${localStorage.getItem('accounts_app')}.com/oauth2/authorize?redirect_uri=http://localhost:3000/&response_type=code&client_id=${this.client_id}&&scope=email%20profile%20spaces&state=0&access_type=offline&grant_type=${grant_type}`
    }

    getAccessToken = (grant_type) => {
        console.log('%c getAccessToken() ', 'background: #0000ff; color: #fff');
        let form = new FormData()
        form.append("grant_type", grant_type)
        form.append("client_id", sessionStorage.getItem('client_id'))
        form.append("client_secret", sessionStorage.getItem('client_secret'))
        form.append("scope", 'spaces profile email')

        if (grant_type === 'authorization_code') {
            form.append("redirect_uri", 'http://localhost:3000/')
            form.append("code", sessionStorage.getItem('code'))
        }

        if (grant_type === 'password') {
            form.append("username", this.username)
            form.append("password", this.password)
        }

        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                url: 'http://localhost:3001/access_token',
                headers:
                {
                    'Content-Type': 'multipart/form-data'
                },
                data: form,
            }).then((res) => {
                console.log(res)
                resolve(res)
            }).catch((err) => {
                console.log(err)
                this.EventEmitter.dispatch('error', { message: 'Could not retrieve the access token', data: JSON.stringify(err, null, " ") })
                reject(err)
            });
        })
    }

    getSpaceInfoFromInviteLink = (params) => {
        console.log('%c getSpaceInfoFromInviteLink()', 'background: #0000ff; color: #fff');
        const { inviteLink, invitationAuth, anonUsername, token, spacePin } = params
        const splitLink = new URL(inviteLink).pathname.split('/')
        this.token = token
        this.spacePin = spacePin

        // 1. Get invite id
        splitLink[1] === 'spaces' && splitLink[2] === 'invites' ? this.inviteId = splitLink[3] : console.log("Could not retrieve inviteId")

        // 2. Check the auth method
        if (invitationAuth === "anonInvitation") {
            console.log(invitationAuth)
            this.authMethodChosen = 'anonToken'
            this.getAnonSpacesToken(anonUsername).then((token) => {
                console.log(token)
                this.token = token
                this.tokenType = 'jwt'
                axios({
                    method: 'GET',
                    url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/invites/${this.inviteId}/join`,
                    headers:
                    {
                        'Authorization': this.tokenType + " " + this.token,
                        'spaces-x-space-password': this.spacePin
                    },
                }).then(res => {
                    console.log(res)
                    this.spaceId = res.data.topic._id
                    this.EventEmitter.dispatch('state', { data: {  spaceId: this.spaceId } })
                    axios({
                        method: 'GET',
                        url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId,
                        headers:
                        {
                            'Authorization': this.tokenType + " " + this.token,
                            'spaces-x-space-password': this.spacePin
                        },
                    }).then((res) => {
                        console.log(res)
                        if (res.data.settings?.confPin?.length) {
                            this.EventEmitter.dispatch('state', { data: { spacePinRequired: true, spaceId: this.spaceId } })
                        }
                    }).catch(err => console.log(err))
                }).catch(err => {
                    if (err.response.status === 403) {
                        this.EventEmitter.dispatch('state', { data: { spacePinRequired: true } })
                        this.EventEmitter.dispatch('error', { message: err.response.message, data: { authModalShown: true } })
                    }
                })
            })
        }
        if (invitationAuth=== "jwtInvitation") {
            this.authMethodChosen = 'jwtToken'
            this.tokenType = 'jwt'
            axios({
                method: 'GET',
                url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/invites/${this.inviteId}/join`,
                headers:
                {
                    'Authorization': this.tokenType + " " + this.token,
                    'spaces-x-space-password': this.spacePin || ""
                },
            }).then(res => {
                this.spaceId = res.data.topic._id
                this.EventEmitter.dispatch('state', { data: {  spaceId: this.spaceId } })
                axios({
                    method: 'GET',
                    url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId,
                    headers:
                    {
                        'Authorization': this.tokenType + " " + this.token,
                        'spaces-x-space-password': this.spacePin
                    },
                }).then((res) => {
                    if (res.data.settings?.confPin?.length) {
                        this.EventEmitter.dispatch('state', { data: { spacePinRequired: true, spaceId: this.spaceId } })
                    }
                }).catch(err => console.log(err))
            }).catch(err => {
                if (err.response.status === 403) {
                    this.EventEmitter.dispatch('state', { data: { spacePinRequired: true } })
                }
                else this.EventEmitter.dispatch('error', { message: err.response.message })
            })
        }
    }

    preJoinSpace = (params) => {
        console.log('%c preJoinSpace()', 'background: #0000ff; color: #fff');
        // Params passed: spaceId, authMethodChosen, anonUsername, client_secret, client_id, token, username, password
        Object.assign(this, params)

        switch (this.authMethodChosen) {
            case "anonToken":
                this.anonAuthChosen = true
                console.log("No authorization token found, retrieving anonymous token...")
                this.getAnonSpacesToken(this.anonUsername).then((token) => {
                    this.token = token
                    this.tokenType = 'jwt'
                    return this.joinSpace();
                }
                );
                break;
            case "jwtToken":
                this.tokenType = 'jwt'
                this.joinSpace();
                break;
            case "authorizationCode":
                sessionStorage.setItem('client_id', this.client_id);
                sessionStorage.setItem('client_secret', this.client_secret);
                sessionStorage.setItem('spaceId', this.spaceId);

                if (sessionStorage.getItem('code') !== null) {
                    this.getAccessToken("authorization_code").then(res => {
                        localStorage.setItem('access_token', res.data.access_token);
                        localStorage.setItem('refresh_token', res.data.refresh_token);
                        localStorage.setItem('id_token', res.data.id_token);
                        this.token = res.data.access_token;
                        // TODO: check if token is dispatched?
                        this.tokenType = 'bearer'
                        return this.joinSpace();
                    }).catch(err => console.log(err))
                }
                else
                    this.redirectToAvayaCloudIdentity('authorization_code')
                break;

            case "password":
                sessionStorage.setItem('client_id', this.client_id);
                sessionStorage.setItem('client_secret', this.client_secret);
                sessionStorage.setItem('spaceId', this.spaceId);
                this.getAccessToken("password").then(res => {
                    localStorage.setItem('access_token', res.data.access_token);
                    localStorage.setItem('refresh_token', res.data.refresh_token);
                    localStorage.setItem('id_token', res.data.id_token);
                    this.token = res.data.access_token;
                    this.tokenType = 'bearer'
                    return this.joinSpace()
                })
                break;
            case "invitation":
                this.joinSpace();
                break;
            default:
                this.EventEmitter.dispatch('error', { message: 'Invalid authMethodChosen property', data: { authModalShown: true } })
                break;
        }
    }

    registerForNetworkEvents = () => {
        if (this.user && this.user.getCalls) {
            const calls = this._user.getCalls();
            const capability = calls.getSetOnNetworkEventsCapability();
            if (capability && capability.isAllowed) {
                const acceptedEvents = this.buildEventsSubscription();
                calls
                    .setOnNetworkEvents(acceptedEvents, this._onNetworkEventsCallback)
                    .then(
                        subscribedEvents => {
                            console.log(subscribedEvents)
                        },
                        error => {
                            console.log(error)
                        }
                    );
            } else {
                console.log('SetOnNetworkEvents capability isAllowed === false')
            }

        }
    }

    _onNetworkEventsCallback = events => {
        if (!events || !events.length) {
            console.warn('event ignored - invalid event parameters', events);
            return;
        }
        events.forEach(event => {
            this.processNetworkEvent(event);
        });
    };


    processNetworkEvent(event) {
        if (this.isActiveSpeakerEvent(event)) {
            this.processActiveSpeakerEvent(event);
        }
    }

    isActiveSpeakerEvent(event) {
        const res =
            event &&
            event.resourceId &&
            event.resourceType === "CONFERENCE" &&
            event.details &&
            event.details.sessions &&
            event.details.sessions.length > 0 &&
            event.trigger === "SUBSCRIPTION" &&
            'ACTIVE_SPEAKER_NOTIFY' === event.name;
        return res;
    }

    processActiveSpeakerEvent(event) {
        let sender;
        let mdsrvSessionId;
        let otherSpeakers = [];
        let topicId;
        event.details.sessions.forEach(speaker => {
            if (speaker.opaqueData && this.isBase64(speaker.opaqueData)) {
                try {
                    const decodedData = this.base64Decode(speaker.opaqueData);
                    if (decodedData && decodedData.length > 0) {
                        const opaqueData = JSON.parse(decodedData);
                        // console.log("speaker information" + opaqueData)
                        const attendee = opaqueData.user;
                        topicId = opaqueData.topicId || topicId;
                        if (!sender && attendee) {
                            mdsrvSessionId = speaker.id;
                            sender = attendee;
                        } else if (attendee) {
                            otherSpeakers.push({
                                mdsrvSessionId: speaker.id,
                                attendee
                            });
                        }
                    }
                } catch (error) {
                    console.log("failed to decode or parse speaker information" + error)

                }
            } else {
                console.log('Invalid speaker information', event);
            }
        });

        if (topicId) {
            this.emitOnActiveSpeakerChanged({
                _id: getGUID(),
                topicId,
                sender,
                content: {
                    mediaSession: { mdsrvSessionId },
                    otherSpeakers
                }
            });
        }
    }

    emitOnActiveSpeakerChanged(event) {
        // console.log('%c Active speaker changed: ', 'background: #ff6a00; color: #fff');
        this.activeSpeaker = event.sender._id
        // console.log(this.activeSpeaker)
        this.EventEmitter.dispatch('state', { data: { activeSpeaker: this.activeSpeaker } })
    }



    startVideoForSpaces = () => {
        console.log('%c startVideoForSpaces()', 'background: #0000ff; color: #fff');
        // Ask Spaces for the MPaaS token for this room
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_app')}.com/api/mediasessions/mpaas/token/` + this.spaceId,
            data: {
                "displayname": "Andrew API",
                "username": "AndrewAPI"
            },
            headers:
            {
                'Authorization': `${this.tokenType} ` + this.token,
            },
        }).then(mpaasInfo => {
            // console.log("MpaaS Info:" + mpaasInfo);
            this.sessionId = mpaasInfo.data.sessionId
            this.mpaasToken = mpaasInfo.data.token;
            // console.log('%c mpaasToken: ', JSON.stringify(this.mpaasToken), 'background: #FFFF00; color: #00FF00');

            if (this.socketConnection) {
                let mediaSessionPayload = {
                    "category": "trackstatus",
                    "content":
                    {
                        "mediaSession":
                        {
                            "audio": !this.audioMuted,
                            "connected": true,
                            "screenshare": this.onScreenShare,
                            "selfMuted": true,
                            "video": !this.videoMuted
                        }
                    },
                    "topicId": this.spaceId,
                };

                this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS', mediaSessionPayload);
                console.log('%c socketConnection.emit SEND_MEDIA_SESSION_EVENTS ', 'background: #0000ff; color: #fff');
            }
        }).catch(err => { this.EventEmitter.dispatch('error', { message: 'Could not retrieve mpaas token', data: JSON.stringify(err, null, " ") }); console.log(err) })

    }


    initiateSpacesCall = () => {
        console.log('%c initiateSpacesCall()', 'background: #0000ff; color: #fff');
        this.EventEmitter.dispatch('state', { data: { showJoiningMeetingSpinner: true } })

        // console.log("Starting conference call. Space Id is " + this.spaceId + ".");
        this.calls = this.user.getCalls()
        this.call = this.calls.createDefaultCall();
        this.call.setVideoMode(window.AvayaClientServices.Services.Call.VideoMode.SEND_RECEIVE);
        this.call.setWebCollaboration(true); // Set to true if you want to enable web collaboration

        this.call.addOnCallIncomingVideoAddRequestDeniedCallback((call) => {
            console.log("IncomingVideoAddRequestDenied");
        });
        this.call.addOnCallFailedCallback((call, callException) => {
            console.log("call failed", callException);
        });
        this.call.addOnCallEndedCallback((call, event) => {
            console.log("call ended");
            console.log(event);
        });
        this.call.addOnCallEstablishingCallback((call) => {
            console.log("call establishing...");
        this.EventEmitter.dispatch('state', { data: { showCallPreviewModal: false } })	

        });

        let acceptedEvents = {
            category: ["CONFERENCE", "SESSION"],
            events: ['ACTIVE_SPEAKER_NOTIFY', 'RESOURCE_NETWORK_STATUS'],
            excludedEvents: []
        }

        this.calls.setOnNetworkEvents(acceptedEvents, this._onNetworkEventsCallback).then((res) => {
            console.log('%c _onNetworkEventsCallback() ', 'background: #ff6a00; color: #fff');
        })

        this.call.addOnCallVideoChannelsUpdatedCallback((call) => {
            console.log("Video channels updated");
            let mediaEngine = this.client.getMediaServices();
            let videoChannels = this.call.getVideoChannels();
            if (videoChannels[0]) {
                switch (videoChannels[0].getNegotiatedDirection()) {
                    case window.AvayaClientServices.Services.Call.MediaDirection.RECV_ONLY:
                        console.log('Media is received but not sent')
                        break;
                    case window.AvayaClientServices.Services.Call.MediaDirection.SEND_ONLY:
                        console.log('Media is sent but no media is received.')
                        break;
                    case window.AvayaClientServices.Services.Call.MediaDirection.SEND_RECV:
                        console.log('Media is sent and received')
                        this.remoteStream = mediaEngine.getVideoInterface().getRemoteMediaStream(videoChannels[0].getChannelId());
                        if (window.AvayaClientServices.Base.Utils.isDefined(this.remoteStream)) {
                            this.startRemoteVideo(this.remoteStream);
                        }
                        break;
                    case window.AvayaClientServices.Services.Call.MediaDirection.INACTIVE:
                        console.log('No media flow. No media is transmitted or received')
                        break;
                    case window.AvayaClientServices.Services.Call.MediaDirection.DISABLE:
                        console.log('Media direction has been disabled.')
                        break;
                    default:
                        break;
                }
            }
        });
        this.call.addOnCallEstablishedCallback((call) => {
            console.log("call established!");

            this.conferenceCall = call;
            this.EventEmitter.dispatch('state', { data: { conferenceCall: this.conferenceCall, showJoiningMeetingSpinner: false } })


            let mediaSessionPayload = {
                "category": "trackstatus",
                "content":
                {
                    "mediaSession":
                    {
                        "audio": !this.audioMuted,
                        "connected": true,
                        "screenshare": this.onScreenShare,
                        "selfMuted": true,
                        "video": !this.videoMuted
                    }
                },
                "topicId": this.spaceId
            };

            console.log("sending MEDIA SESSION EVENT");
            this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS', mediaSessionPayload);

            let presencePayload = {
                "category": "app.event.presence.party.online",
                "content":
                {
                    "desktop": false,
                    "idle": false,
                    "mediaSession":
                    {
                        "audio": !this.audioMuted,
                        "connected": true,
                        "phone": false,
                        "screenshare": false,
                        "selfMuted": true,
                        "video": !this.videoMuted
                    },
                    "offline": false,
                    "role": "guest"
                },
                "topicId": this.spaceId
            };
            this.socketConnection.emit('SEND_PRESENCE_EVENT', presencePayload);
            this.getActiveMeeting()
            try {
                this.collaborations = this.user.getCollaborations()
                this.collaboration = this.collaborations.getCollaborationForCall(this.call.getCallId());
                this.collaboration.addOnCollaborationStartedCallback(() => {
                    console.log("Collaboration Started...");
                });
                this.collaboration.addOnCollaborationInitializedCallback(() => {
                    console.log("Collaboration Initialized...");
                });
                this.collaboration.addOnCollaborationServiceAvailableCallback(() => {
                    console.log("Collaboration Service Available...");
                });
                this.collaboration.getContentSharing().addOnContentSharingEndedCallback(() => {
                    console.log("Content Sharing Ended...");
                    this.toggleScreenShare(false)
                    this.startLocalVideo();
                    this.onScreenShare = false
                    this.EventEmitter.dispatch('state', { data: { onScreenShare: this.onScreenShare } })
                });
                this.collaboration.getContentSharing().onScreenSharingCapabilityChangedCallback((res) => {
                    console.log("Screen Sharing Capability Changed " + res)
                })
            } catch (err) {
                console.log("Error: " + err.message);
            }
            this.startLocalVideo();

            // TO BE KEPT FOR FUTURE REFERENCE

            // this.call.addOnAnswerCallRequestedCallback((call) => {
            //     console.log('addOnAnswerCallRequestedCallback')
            // });
            // this.call.addOnAudioStreamUpdatedCallback((call) => {
            //     console.log('addOnAudioStreamUpdatedCallback')
            // });
            // this.call.addOnCallAudioMuteStatusChangedCallback((call) => {
            //     console.log('addOnCallAudioMuteStatusChangedCallback')
            // });
            // this.call.addOnCallConferenceStatusChangedCallback((call) => {
            //     console.log('addOnCallConferenceStatusChangedCallback')
            // });
            // this.call.addOnCallSpeakerMuteStatusChangedCallback((call) => {
            //     console.log('addOnCallSpeakerMuteStatusChangedCallback')
            // });
            // this.call.addOnCallStartedCallback((call) => {
            //     console.log('addOnCallStartedCallback')
            // });
            // this.call.addOnCallVideoChannelsUpdatedCallback((call) => {
            //     console.log('addOnCallVideoChannelsUpdatedCallback')
            // });
            // this.call.addOnCallVideoRemovedRemotelyCallback((call) => {
            //     console.log('addOnCallVideoRemovedRemotelyCallback')
            // });

        });
        this.call.addOnCallConferenceStatusChangedCallback((call) => {
            this.collaboration.addOnCollaborationServiceAvailableCallback(() => {
                console.log("addOnCollaborationServiceAvailableCallback");
                this.collaboration.getContentSharing().addOnContentSharingStartedCallback(() => {
                    // console.log("addOnContentSharingStartedCallback");
                    this.contentSharingRenderer = new window.AvayaClientServices.Renderer.Konva.KonvaContentSharingRenderer()
                    if (this.onScreenShare) {
                        console.log("Sending screenshare");
                        document.querySelector("#localVideoElement").srcObject = this.collaboration.getContentSharing().getOutgoingScreenSharingStream();
                        // this.EventEmitter.dispatch('state', { data: { onScreenShare: this.onScreenShare } })
                    } else {
                        console.log("Receiving screenshare");
                        this.contentSharingRenderer.init(this.collaboration.getContentSharing(), 'screenReceiveFrame');
                    }

                });
                this.collaboration.getContentSharing().addOnContentSharingEndedCallback(() => {
                    // console.log("addOnContentSharingEndedCallback");
                });
            });
            this.collaboration.addOnCollaborationServiceUnavailableCallback(() => {
                console.log("addOnCollaborationServiceUnavailableCallback");
            });
            console.log("addOnCallConferenceStatusChangedCallback");
            this.collaboration.start().catch((e) => {
                console.log("Error starting collaboration: " + e);
            });
        });
        this.videoMuted && this.call.muteVideo();
        this.audioMuted && this.call.muteAudio();
        this.EventEmitter.dispatch('state', { data: { showCallPreviewModal: false } })	
        this.call.start();
    }

    joinDirectSpace = (userId) => {
        console.log('%c joinDirectSpace()', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/direct/${this.anonAuthChosen ? 'anonymous' : 'user'}/${userId}`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then((res) => {
            this.spaceId = res.data.data[0]._id
            this.joinSpace()
        })
    }

    getMySpaces = () => {
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/spaces?size=50&topictype=group`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then((res) => {
            res.data.data.map(item => this.mySpaces = { ...this.mySpaces, [item._id]: item })
            this.EventEmitter.dispatch('state', { data: { mySpaces: this.mySpaces } })
        })
    }

    getColleaguesAndDMList = (query) => {
        this.colleaguesAndDMList = {}
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/people/withdm?search=${query}`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then((res) => {
            res.data.data.filter(item => this.colleaguesAndDMList[item._id] = item)
            axios({
                method: 'GET',
                url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/colleagues?search=${query}`,
                headers:
                {
                    'Authorization': this.tokenType + " " + this.token,
                },
            }).then((res) => {
                this.colleaguesAndDMList = res.data.data.filter(item => this.colleaguesAndDMList[item._id] = item)
                if (this.colleaguesAndDMList[this.me._id]) delete this.colleaguesAndDMList[this.me._id]
                this.EventEmitter.dispatch('state', { data: { colleaguesAndDMList: this.colleaguesAndDMList } })
            })
        })
    }

    joinSpace = (passedSpaceId) => {
        // TODO : this.inviteURL = ????????
        console.log('%c joinSpace()', 'background: #0000ff; color: #fff');
        // Create client object
        this.client = new window.AvayaClientServices();

        this.popCurrentSpace()
        this.getUserInfo()
        this.getMySpaces()
        this.getUserSettings()
        this.getActiveMeeting()

        if (passedSpaceId !== undefined) this.spaceId = passedSpaceId
        let connectionPayload =
        {
            query: "token=" + this.token + `&tokenType=${this.tokenType === 'bearer' ? 'oauth' : 'jwt'}`,
            transports: ['websocket']
        };
        axios({
            method: 'GET',
            url: this.inviteURL || `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + `/join`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
        }).then((res) => {
            this.socketConnection = io.connect(this.socketURL, connectionPayload)
            this.initiateSocketListeners()
            this.spaceTitle = res.data.topic?.title || res.data.topic.type
            this.EventEmitter.dispatch('state', { data: { spaceTitle: this.spaceTitle, showSubscribingSpinner: false } })
            // Ask Spaces for the MPaaS token for this room
            axios({
                method: 'GET',
                url: `https://${localStorage.getItem('spaces_app')}.com/api/mediasessions/mpaas/token/` + this.spaceId,
                data: {
                    "displayname": "Andrew API",
                    "username": "AndrewAPI"
                },
                headers:
                {
                    'Authorization': `${this.tokenType} ` + this.token,
                },
            }).then(mpaasInfo => {
                // console.log("MpaaS Info:" + mpaasInfo);
                this.sessionId = mpaasInfo.data.sessionId
                this.mpaasToken = mpaasInfo.data.token;
                // console.log('%c mpaasToken: ', JSON.stringify(this.mpaasToken), 'background: #FFFF00; color: #00FF00');

                if (this.socketConnection) {
                    let mediaSessionPayload = {
                        "category": "trackstatus",
                        "content":
                        {
                            "mediaSession":
                            {
                                "audio": !this.audioMuted,
                                "connected": true,
                                "screenshare": this.onScreenShare,
                                "selfMuted": true,
                                "video": !this.videoMuted
                            }
                        },
                        "topicId": this.spaceId,
                    };

                    this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS', mediaSessionPayload);
                    console.log('%c socketConnection.emit SEND_MEDIA_SESSION_EVENTS ', 'background: #0000ff; color: #fff');

                    // Create configuration with the media service context passed in.
                    const userConfiguration = {
                        mediaServiceContext: this.mpaasToken, // <- Set the MPaaS token here as the service context
                        callUserConfiguration:
                        {
                            videoEnabled: true // A boolean value indicating whether application allows video or not.
                        },
                        wcsConfiguration: {
                            enabled: true // Boolean value indicating whether the WCS provider is enabled.
                        },
                        collaborationConfiguration: {
                            imageQuality: 10, // Default image quality used in content sharing. Web Collaboration Server may override this value. Value ranges from 1 to 10, where 10 is highest image quality.
                            contentSharingWorkerPath: `/libs/AvayaClientServicesWorker.js`,
                        },
                    };

                    // Create user object
                    this.user = this.client.createUser(userConfiguration);
                    // start() method will asynchronously set up connection to the MPaaS web user agent.
                    // Asynchronous callback information is omitted here for simplicity.
                    this.user.start().then(() => {
                        // console.log("user.start success");
                    }
                    ).catch(err => this.EventEmitter.dispatch('error', { message: 'user.start failure', data: JSON.stringify(err, null, " ") })
                    )
                }
            }).catch(err => { this.EventEmitter.dispatch('error', { message: 'Could not retrieve mpaas token', data: JSON.stringify(err, null, " ") }); console.log(err) })
            return this.socketConnection
        }).catch(err => {
            this.EventEmitter.dispatch('error', { message: err.message, data: JSON.stringify(err, null, " ") })
            this.EventEmitter.dispatch('state', { message: 'Connection attempt timeout', data: { showSubscribingSpinner: false } })
        })
    }

    getUserSettings = () => {	
        console.log('%c getUserSettings() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'GET',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/settings`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
        }).then((res) => {	
            this.userSettings = res.data.data[0]	
            console.log(this.userSettings)	
            this.EventEmitter.dispatch('state', { data: { userSettings: this.userSettings, isNoiseReductionEnabled: !this.userSettings.disableNoiseReductionOnJoin } })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve user settings', data: JSON.stringify(err, null, " ") }))	
    }	
    
    toggleUserSettings = (params) => {	
        console.log('%c toggleUserSettings() ', 'background: #0000ff; color: #fff');	
        const {disablePreviewWhenJoin, muteaudio, mutevideo} = params
        console.log(params)
        this.userSettings = { ...this.userSettings, disablePreviewWhenJoin, muteaudio, mutevideo }	
        this.EventEmitter.dispatch('state', { data: { userSettings: this.userSettings } })	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/users/me/settings/${this.userSettings._id}`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
            data: {	
                disablePreviewWhenJoin: !disablePreviewWhenJoin,	
                muteaudio,	
                mutevideo	
            }	
        }).then((res) => {	
            console.log(res)	
            this.getUserSettings()	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not change user settings', data: JSON.stringify(err, null, " ") }))	
    }	

    toggleNoiseReduction = (isNoiseReductionEnabled) => {	
        console.log('%c toggleNoiseReduction() ', 'background: #0000ff; color: #fff');	
        this.isNoiseReductionEnabled = !isNoiseReductionEnabled
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/attendees/${this.me.aType}/${this.me._id}/operation`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
            data: {	
                "isNoiseReductionEnabled": !isNoiseReductionEnabled,	
                mediaSessionId: this.sessionId	
            }	
        }).then((res) => {	
            this.isNoiseReductionEnabled = !isNoiseReductionEnabled
            this.EventEmitter.dispatch('state', { data: { "isNoiseReductionEnabled": this.isNoiseReductionEnabled } })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not toggle AI Noise reduction', data: JSON.stringify(err, null, " ") }))	
    }	

    getSpaceTasks = () => {	
        axios({	
            method: 'GET',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/tasks',	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
        }).then((res) => {	
            this.spaceTasks = res.data.data.reverse()
            this.EventEmitter.dispatch('state', { data: { spaceTasks: this.spaceTasks} })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve tasks from a specified space', data: JSON.stringify(err, null, " ") }))	
    }	

    getSpacePosts = () => {	
        axios({	
            method: 'GET',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/ideas',	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
        }).then((res) => {	
            this.spacePosts = res.data.data
            this.EventEmitter.dispatch('state', { data: { spacePosts: this.spacePosts} })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve posts from a specified space', data: JSON.stringify(err, null, " ") }))	
    }	

    getSpaceMeetings = () => {	
        axios({	
            method: 'GET',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/meetings',	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
                'Accept': 'application/json',	
                'Content-Type': 'application/json',	
            },	
        }).then((res) => {	
            this.spaceMeetings = res.data.data	
            this.EventEmitter.dispatch('state', { data: { spaceMeetings: this.spaceMeetings } })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not retrieve meetings from a specified space', data: JSON.stringify(err, null, " ") }))	
    }


    getSpaceContent = () => {
        console.log('%c getSpaceContent() ', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/topics/` + this.spaceId + '/messages/byref?=30',
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then((res) => {
            this.spaceContent = res.data.data.reverse()
            this.EventEmitter.dispatch('state', { data: { spaceContent: this.spaceContent } })
        })
    }

    initiateSocketListeners = () => {
        console.log('%c initiateSocketListeners()', 'background: #0000ff; color: #fff');
        this.socketConnection.on('PRESENCE_EVENT_RESPONSE', (res) => {
            const currentSpaceContent = res.topicId === this.spaceId
            if(currentSpaceContent) {
                if (res.category === 'app.event.presence.party.online') {
                    console.log(`PRESENCE_EVENT_RESPONSE: ${res.category}`);
                    // Push the user into members list
                    if (Object.keys(this.spaceMembers).includes(res.sender._id) === false) this.spaceMembers = { ...this.spaceMembers, [res.sender._id]: res.sender }
                    this.spaceMembers[res.sender._id] = { ...this.spaceMembers[res.sender._id], content: { ...res.content } }
                    this.EventEmitter.dispatch('state', {
                        data: { spaceMembers: this.spaceMembers }
                    })
                }

                if (res.category === 'party.typing') {
                    this.EventEmitter.dispatch('state', { data: { partyTyping: true }})
                    setTimeout(() => {
                        this.EventEmitter.dispatch('state', { data: { partyTyping: false } })
                    }, 2000);
                }
                
                if (res.category === 'app.event.presence.party.leaves') {
                    if (this.spaceMembers[res.sender._id])	{
                        delete this.spaceMembers[res.sender._id]
                        this.EventEmitter.dispatch('state', { data: { spaceMembers: this.spaceMembers } })	
                    }	
                }
            }
              
        });
        this.socketConnection.on('connect', () => {
            // console.log("Socket connection success!");
            let spaceToSubscribe = {
                channel:
                {
                    _id: this.spaceId,
                    type: 'topic',
                    password: this.spacePin
                }
            };
            this.socketConnection.emit('SUBSCRIBE_CHANNEL', spaceToSubscribe);
        });

        this.socketConnection.on("CHANNEL_SUBSCRIBED", (channelInfo) => {
            // this.sendMessage("Spaces sample app is online"); // Send chat message to Space
            this.EventEmitter.dispatch('state', { data: { channelSubscribed: true } })
            let presencePayload = {
                "category": "app.event.presence.party.online",
                "content":
                {
                    "desktop": false,
                    "idle": false,
                    "mediaSession":
                    {
                        "audio": !this.audioMuted,
                        "video": !this.videoMuted,
                        "connected": false,
                        "phone": false,
                        "screenshare": false,
                        "selfMuted": true,
                    },
                    "offline": false,
                    "role": "guest"
                },
                "topicId": this.spaceId,
                "loopbackMetadata": "some metadata"
            };
            this.socketConnection.emit('SEND_PRESENCE_EVENT', presencePayload);
            this.getSpaceMemberList()
            this.getSpaceContent()
        });

        this.socketConnection.on('connect_error', (err) => {
            console.log(`Socket connection error:`);
            console.log(err);
        });

        this.socketConnection.on('error', (err) => {
            console.log(`Socket error:`);
            console.log(err)
        });

        this.socketConnection.on('disconnect', () => {
            console.log('Socket disconnected.');
        });


        this.socketConnection.on('MEDIA_SESSION_RESPONSE', (res) => {
            const currentSpaceContent = res.topicId === this.spaceId

            if (currentSpaceContent) {
                if (res.category === "app.event.recording.started") {
                    this.recordingId = res.content.recordings[0]._id;
                }

                if (res.category === "tracksstatus") {	
                    if (this.spaceMembers[res.sender._id] !== undefined &&	
                        this.spaceMembers[res.sender._id].content !== undefined) {	
                        this.spaceMembers[res.sender._id].content.mediaSession = res.content.mediaSession	
                        this.EventEmitter.dispatch('state', { data: { spaceMembers: this.spaceMembers } })	
                    }	
                }

                if (res.category === 'app.event.attendee.added') {	
                    this.getActiveMeeting()	
                }	
                if (res.category === 'app.event.all.parties.muted') {	
                    res.content.exclude.forEach(item => {	
                        if (item.attendeeId === this.me._id) {	
                            return	
                        } else {	
                            let mediaSessionPayload = {	
                                "topicId": this.spaceId,	
                                "category": "tracksstatus",	
                                "sycnhedWithServer": true,	
                                "content": {	
                                    "mediaSession": {	
                                        "audio": false,	
                                        "video": !this.videoMuted,	
                                        "connected": true,	
                                        "isCollabOnly": false,	
                                        "mdsrvSessionId": this.sessionId,	
                                        "joinTime": new Date()	
                                    },	
                                    "streamId": this.remoteStream.id,	
                                    "subscribeTime": new Date()	
                                },	
                                "sender": this.sender,	
                                "data": [],	
                                "created": "2020-12-28T11:58:55.897Z",	
                                "version": "1.1"	
                            }	
                            this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS', mediaSessionPayload);	
                        }	
                    })	
                }

                if (res.category === 'app.event.meeting.permission.requested') {	
                    this.userRaisedHand = res.content.targetId
                    this.EventEmitter.dispatch('state', { data: { userRaisedHand: this.userRaisedHand } })	
                } 

                if (res.category === 'app.event.attendee.left') {	
                    delete this.spaceMembers[res.content.attendee._id]	
                    this.EventEmitter.dispatch('state', {	
                        data: { spaceMembers: this.spaceMembers },	
                    })	
                }	
                if (res.category === 'app.event.meeting.ended') {	
                    if (this.meetingData !== null) {	
                    this.meetingData = null	
                    this.EventEmitter.dispatch('state', { data: { meetingData: this.meetingData } })	
                    this.leaveMeeting()	
                    }	
                    this.meetingData = res.content	
                    this.EventEmitter.dispatch('state', { data: { meetingData: this.meetingData } })	
                }	
                if (res.category === 'app.event.meeting.started') {	
                    this.getActiveMeeting()	
                }

                // if (res.sender !== undefined && res.sender._id in this.spaceMembers) this.spaceMembers[res.sender._id].content = res.content;
                // this.EventEmitter.dispatch('state', {
                //     data: { spaceMembers: this.spaceMembers },
                // })
            }
        })


        this.socketConnection.on('MESSAGE_SENT', (res) => {
            const currentSpaceContent = res.topicId === this.spaceId
            const lastMessage = this.spaceContent[this.spaceContent.length - 1]
            // Prevent pushing message duplicate and leaks from other spaces
            if (lastMessage?.created !== res.created && currentSpaceContent) {
                this.spaceContent = [...this.spaceContent, res]
            }
            this.EventEmitter.dispatch('state', { data: { spaceContent: this.spaceContent }})
        });

        this.socketConnection.on('SEND_MESSAGE_FAILED', (error) => {
            console.log('SEND_MESSAGE_FAILED');
            console.log(error)
        });
    }

    extractMentions = (text = '') => {
     const MENTION_REGEXP = /<mention[^>]*id="([^"]+)"[^>]*type="([^"]+)"[^>]*value="([^"]+)"[^>]*>/g;
        text = text.replace(/<pre.*?>(.|\n)*?<\/pre>/g, '');
        const mentions = [];
        const mentionMatches = [...(text || '').matchAll(MENTION_REGEXP)];
        mentionMatches.forEach((mentionMatch = []) => {
            mentions.push({
            id: mentionMatch[1],
            type: mentionMatch[2],
            title: mentionMatch[3]
            });
        });
        return mentions;
};

    sendMessage = (messageToSend) => {
        console.log('%c sendMessage() ', 'background: #0000ff; color: #fff');
        const userMentions = this.extractMentions(messageToSend);
        const message = {
            content:
            {
                bodyText: messageToSend
            },  
            category: 'chat',
            topicId: this.spaceId,
            mentions: userMentions
        };
        this.socketConnection.emit('SEND_MESSAGE', message)
    }

    uploadFile = (selectedFile) => {
        return new Promise((resolve, reject) => {
            console.log('%c uploadFile() ', 'background: #0000ff; color: #fff');
            let form = new FormData()
            form.append("selectedFile", selectedFile)
            form.append("tokenType", this.tokenType)
            form.append("token", this.token)

            
            let config = {
                "headers": {
                    'Content-Type': 'multipart/form-data'
                }
            }
            axios.post('http://localhost:3001/upload_file', form, config)
            .then(res => resolve(res)).catch((err) => {
                console.log(err)
                this.EventEmitter.dispatch('error', { message: 'Could not upload to Google storage', data: JSON.stringify(err, null, " ") })
                reject(err)
                });
            })
    }
        
    sendPost = (params) => {
        console.log('%c sendPost() ', 'background: #0000ff; color: #fff');
        const {postName, postDescription, selectedFile} = params

        this.post = {
            chatMessages: {},
            category: 'idea',
            content:
            {
                assignees: [],
                bodyText: postName,
                description: postDescription,
                status: "pending"
            },
            topicId: this.spaceId,
            version: "1.1"
        };

        if (typeof selectedFile !== 'undefined') {
            this.uploadFile(selectedFile).then((res) => {
                this.post = {
                    ...this.post, content: {
                        ...this.post.content, data: [{
                            "fileId": res.data,
                            "fileSize": selectedFile.size,
                            "fileType": selectedFile.type,
                            "icon": "",
                            "name": selectedFile.name,
                            "provider": "native",
                            "providerFileType": selectedFile.type
                        }]
                    }
                };

                this.socketConnection.emit('SEND_MESSAGE', this.post);
                this.EventEmitter.dispatch('state', {
                    message: "", data: { showModalPostCreate: false }
                })
                this.getSpacePosts();
            })
            return
        }
        else
        console.log(this.post)
        this.socketConnection.emit('SEND_MESSAGE', this.post)
        this.EventEmitter.dispatch('state', {
            data: { showModalPostCreate: false }
        })
        this.getSpacePosts();
    }

    sendEditedPost = (params) => {
        console.log('%c sendEditedPost() ', 'background: #0000ff; color: #fff');
        const { editedPost } = params
        console.log(params)

            return axios({
                method: 'POST',
                url: 'http://localhost:3001/edit_post',
                data: {   
                    editedPost,
                    token: this.token,
                    tokenType: this.tokenType,
                }})
            .then((res) => {
            console.log(res)
            this.showModalPostEdit = false
            this.EventEmitter.dispatch('state', { data: { showModalPostEdit: this.showModalPostEdit } })
            this.getSpacePosts();
        }).catch((err) => {
            console.log(err)
        })
    }

    sendEditedTask = (params) => {
        console.log('%c sendEditedTask() ', 'background: #0000ff; color: #fff');
        const { editedTask } = params
        console.log(params)

            return axios({
                method: 'POST',
                url: 'http://localhost:3001/edit_task',
                data: {   
                    editedTask,
                    token: this.token,
                    tokenType: this.tokenType,
                }})
            .then((res) => {
            console.log(res)
            this.showModalTaskEdit = false
            this.EventEmitter.dispatch('state', { data: { showModalTaskEdit: this.showModalTaskEdit } })
            this.getSpaceTasks();
        }).catch((err) => {
            console.log(err)
        })
    }

    sendTask = (params) => {
    const {taskName, taskDescription, dueDate, selectedFile} = params

        console.log('%c sendTask() ', 'background: #0000ff; color: #fff');
        this.task = {
            category: 'task',
            chatMessages: {},
            content:
            {
                assignees: [],
                bodyText: taskName,
                description: taskDescription,
                dueDate,
                status: "pending"
            },
            topicId: this.spaceId,
            version: "1.1"
        };

        if (typeof selectedFile !== 'undefined') {
            this.uploadFile(selectedFile).then((res) => {
                this.task = {
                    ...this.task, content: {
                        ...this.task.content, data: [{
                            "fileId": res.data,
                            "fileSize": selectedFile.size,
                            "fileType": "document", // TO-DO: make it dynamic
                            "icon": "",
                            "name": selectedFile.name,
                            "provider": "native",
                            "providerFileType": selectedFile.type
                        }]
                    }
                };

                this.socketConnection.emit('SEND_MESSAGE', this.task);
                this.EventEmitter.dispatch('state', {
                    data: { showModalTaskCreate: false }
                })
                this.getSpaceTasks();
            })
            return
        }
        else
        this.socketConnection.emit('SEND_MESSAGE', this.task)
        this.EventEmitter.dispatch('state', {
            data: { showModalTaskCreate: false }
        })
        this.getSpaceTasks()

    }

    popCurrentSpace = () => {
        console.log('%c popCurrentSpace() ', 'background: #0000ff; color: #fff');
        if (this.socketConnection !== undefined) {
            let payload = {
                "channel":
                {
                    "type": "topic",
                    "_id": this.spaceId
                }
            };
            this.socketConnection.emit('UNSUBSCRIBE_CHANNEL', payload);
            console.log('%c socketConnection.emit UNSUBSCRIBE_CHANNEL ', 'background: #0000ff; color: #fff');

            let presencePayload = {
                "category": "app.event.presence.party.leaves",
                "content":
                {
                    "desktop": false,
                    "idle": false,
                    "mediaSession":
                    {
                        "audio": false,
                        "connected": false,
                        "phone": false,
                        "screenshare": false,
                        "selfMuted": true,
                        "video": false
                    },
                    "offline": false,
                    "role": "guest"
                },
                "topicId": this.spaceId,
                "loopbackMetadata": "some metadata"
            };
            this.socketConnection.emit('SEND_PRESENCE_EVENT', presencePayload);
            console.log('%c socketConnection.emit SEND_PRESENCE_EVENT ', 'background: #0000ff; color: #fff');


            this.spaceMembers = {}
            this.spacePosts = [];
            this.spaceTasks = [];
            this.spaceMeetings = [];
            this.spaceContent = [];
            this.spaceTitle = ''
            this.channelSubscribed = false
            this.EventEmitter.dispatch('state', {
                data: {
                    spaceMembers: this.spaceMembers,
                    spacePosts: this.spacePosts,
                    spaceTasks: this.spaceTasks,
                    spaceMeetings: this.spaceMeetings,
                    spaceTitle: this.spaceTitle,
                    spaceContent: this.spaceContent,
                    channelSubscribed: this.channelSubscribed
                }
            })
            this.leaveMeeting()
        }
    }

    leaveMeeting = () => {
        if (this.conferenceCall) {
            this.conferenceCall.end();
            this.conferenceCall = null;
            this.onScreenShare = false;
            this.EventEmitter.dispatch('state', { data: { conferenceCall: this.conferenceCall, onScreenShare: this.onScreenShare } })
            this.stopLocalVideo();
            this.stopRemoteVideo();
            this.onScreenShare && this.toggleScreenShare(false);
        }
    }

    endMeeting = () => {
        console.log('%c endMeeting() ', 'background: #0000ff; color: #fff');
        axios({
            method: 'POST',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/end`,
            headers:
            {
                'Authorization': this.tokenType + " " + this.token,
            },
            data:
            {
                "spaceId": this.spaceId,
                "meetingId": this.meetingId
            }
        }).then((res) => {
            console.log(res)
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not end the meeting', data: JSON.stringify(err, null, " ") }))
        this.leaveMeeting()
        this.meetingData = null
        this.EventEmitter.dispatch('state', { data: { meetingData: this.meetingData } })
        this.getSpaceContent()
    }


    toggleMedia = (mediaTarget, state) => {
        console.log('%c toggleMedia() ', 'background: #0000ff; color: #fff');
        const toggleMicrophone = mediaTarget === "microphone";
        const toggleCamera = mediaTarget === "camera";

        if (this.call === null) {
            if (toggleCamera) {
                this.videoMuted = !state;
                this.EventEmitter.dispatch('state', { data: { videoMuted: this.videoMuted } })
            }
            if (toggleMicrophone) {
                this.audioMuted = !state
                this.EventEmitter.dispatch('state', { data: { audioMuted: this.audioMuted } })
            }
            return
        }
        

        if (this.call !== null) {
            if (toggleCamera ) {
                if (!state) {
                    this.call.muteVideo()
                } else this.call.unmuteVideo()
                this.videoMuted = !state
                this.EventEmitter.dispatch('state', { data: { videoMuted: this.videoMuted } })
            }
            if (toggleMicrophone) {
                if (!state) {
                    this.call.muteAudio()
                } else this.call.unmuteAudio()
            this.audioMuted = !state
            this.EventEmitter.dispatch('state', { data: { audioMuted: this.audioMuted } })
            }           

        let mediaSessionPayload = {
            "topicId": this.spaceId,
            "category": "tracksstatus",
            "sycnhedWithServer": true,
            "content": {
                "mediaSession": {
                    "audio": !this.audioMuted,
                    "video": !this.videoMuted,
                    "connected": true,
                    "isCollabOnly": false,
                    "mdsrvSessionId": this.sessionId,
                },
                "streamId": this.remoteStream.id,
            },
            "sender": this.sender,
            "data": [],
            "version": "1.1"
        }
        this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS', mediaSessionPayload)
        
        axios({	
            method: 'POST',	
            url: `http://localhost:3001/media_toggle`,	
            data: {
                videoMuted: this.videoMuted,
                audioMuted: this.audioMuted,
                mdsrvSessionId: this.sessionId,
                spaceId: this.spaceId,
                attendeeType: this.me.aType,
                userId: this.me._id,
                meetingId: this.meetingId,
                tokenType: this.tokenType,
                token: this.token
            }
        }).then(res => {
            // console.log(res)
        }).catch(err => {
            console.log(err)
            this.EventEmitter.dispatch('error', { message: 'Could not toggle media state', data: JSON.stringify(err, null, " ") })
        })
    }
    }

    muteAll = () => {	
        console.log('%c muteAll() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/muteall`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
            },	
            data: { 'exclusionList': [] },	
        }).then((res) => {	
            console.log(res)	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not initiate mute all', data: JSON.stringify(err, null, " ") }))	
    }	
    
    raiseHand = () => {	
        console.log('%c raiseHand() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/attendees/raisedhands`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
            },	
            data: { 'requestedPermissions': ['PERMISSION_UNMUTE'] },	
        }).then((res) => {	
            console.log(res)	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not raise hand', data: JSON.stringify(err, null, " ") }))	
    }	

    acceptRaisedHand = (params) => {	
        const { attendeeId, attendeeType } = params.val
        console.log('%c acceptRaisedHand() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/attendees/raisedhands/accept`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
            },	
            data: { 'grantedPermissions': ['PERMISSION_UNMUTE'], attendeeId, attendeeType },	
        }).then((res) => {	
            this.EventEmitter.dispatch('state', { data: { userRaisedHand: "" } })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not accept raised hand', data: JSON.stringify(err, null, " ") }))	
    }	

    lowerRaisedHand = (params) => {	
        const { attendeeId, attendeeType } = params
        console.log('%c lowerRaisedHand() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/attendees/raisedhands/lower`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
            },	
            data: { 'rejectedPermissions': ['PERMISSION_UNMUTE'], attendeeId, attendeeType },	
        }).then((res) => {	
            this.EventEmitter.dispatch('state', { data: { userRaisedHand: "" } })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not lower raised hand', data: JSON.stringify(err, null, " ") }))	
    }	
     
    muteMember = (params) => {
        const {attendeeId, attendeeType} = params
        console.log('%c muteMember() ', 'background: #0000ff; color: #fff');	
        axios({	
            method: 'POST',	
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/${this.spaceId}/meetings/${this.meetingId}/attendees/${attendeeType}/${attendeeId}/mediastate`,	
            headers:	
            {	
                'Authorization': this.tokenType + " " + this.token,	
            },	
            data: { audio: false },	
        }).then((res) => {	
            this.EventEmitter.dispatch('state', { message: "Succesffuly muted another participant" })	
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not mute another participant', data: JSON.stringify(err, null, " ") }))
    }

    presetCall = () => {	
        console.log('%c presetCall() ', 'background: #0000ff; color: #fff');	
        if (this.userSettings.disablePreviewWhenJoin) {	
            return this.initiateSpacesCall()	
        } else {	
            let awaitPreviewModal = new Promise((resolve, reject) => {	
                resolve(this.EventEmitter.dispatch('state', { data: { showCallPreviewModal: true } }))	
            })	
            awaitPreviewModal.then(() => {	
                let video = document.querySelector("#previewVideoElement");	
                if (navigator.mediaDevices.getUserMedia) {	
                    navigator.mediaDevices.getUserMedia(	
                        {	
                            video: true	
                        })	
                        .then((stream) => {	
                            video.srcObject = stream;	
                        })	
                        .catch((err) => {	
                            console.log("Something went wrong!" + err);	
                            console.log(video)	
                            this.EventEmitter.dispatch('error', { message: 'Could not start local video ' + err, data: "Although the user granted permission to use the matching devices, a hardware error occurred at the operating system, browser, or Web page level. It's likely your camera is already being used" })	
                        });	
                }	
            }).catch((err) => {	
                console.log("Something went wrong!" + err);	
                this.EventEmitter.dispatch('error', { message: 'Could not start preview video ' + err, data: err })	
            });	
        }	
    }

    startRemoteVideo = (stream) => {
        console.log('%c startRemoteVideo() ', 'background: #0000ff; color: #fff');
        let video = document.querySelector("#remoteVideoElement");
        video.srcObject = stream;
    }

    stopRemoteVideo = () => {
        console.log('%c stopRemoteVideo() ', 'background: #0000ff; color: #fff');
        let video = document.querySelector("#remoteVideoElement");
        video.srcObject = null;

        const presencePayload = {
            "topicId": this.spaceId,
            "category": "video.end",
            "sender": this.sender
        }
        this.socketConnection.emit('SEND_MEDIA_SESSION_EVENTS ', presencePayload);
    }

    startLocalVideo = () => {
        console.log('%c startLocalVideo() ', 'background: #0000ff; color: #fff');
        let video = document.querySelector("#localVideoElement");
        if (navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia(
                {
                    video: true
                })
                .then((stream) => {
                    video.srcObject = stream;
                })
                .catch((err) => {
                    console.log("Something went wrong!" + err);
                    this.EventEmitter.dispatch('error', { message: 'Could not start local video ' + err, data: "Although the user granted permission to use the matching devices, a hardware error occurred at the operating system, browser, or Web page level. It's likely your camera is already being used" })
                });
        }
    }

    stopLocalVideo = () => {
        console.log('%c stopLocalVideo()', 'background: #0000ff; color: #fff');
        let video = document.querySelector("#localVideoElement");

        let stream = video.srcObject;
        let tracks = stream.getTracks();

        for (let i = 0; i < tracks.length; i++) {
            let track = tracks[i];
            track.stop();
        }

        video.srcObject = null;
    }

    displayMicDevices = () => {
        console.log('%c displayMicDevices()', 'background: #0000ff; color: #fff');
        let audioInterface = this.client.getMediaServices().getAudioInterface().getInputInterface();
        audioInterface.getAvailableDevices().forEach((device) => {

            return this.micDevices.includes(device) ? null :
                (this.addMicrophone(device._label, device._deviceId),
                    this.micDevices.push(device))
        })
        console.log(this.micDevices)
        this.EventEmitter.dispatch('state', { data: { micDevices: this.micDevices } })
    };


    displayCameraDevices = () => {
        console.log('%c displayCameraDevices()', 'background: #0000ff; color: #fff');
        let videoInterface = this.client.getMediaServices().getVideoInterface();
        videoInterface.getAvailableDevices().forEach((device) => {
            return this.cameraDevices.includes(device) ? null :
                (this.addCamera(device._label, device._deviceId),
                    this.cameraDevices.push(device));
        })
        console.log(this.cameraDevices)
        this.EventEmitter.dispatch('state', { data: { cameraDevices: this.cameraDevices } })
    }

    displaySpeakerDevices = () => {
        console.log('%c displaySpeakerDevices()', 'background: #0000ff; color: #fff');
        let speakersInterface = this.client.getMediaServices().getAudioInterface().getOutputInterface();
        speakersInterface.getAvailableDevices().forEach((device) => {
            if ((device._deviceId !== "default") && (device._deviceId !== "communications")) {
                return this.speakerDevices.includes(device) ? null :
                    (this.addSpeaker(device._label, device),
                        this.speakerDevices.push(device));
            }
        });
        console.log(this.speakerDevices)
        this.EventEmitter.dispatch('state', { data: { speakerDevices: this.speakerDevices } })
    };

    setMicrophone = (device) => {
        console.log(this.micOptions)
        console.log(this.micDevices)
        console.log('%c setMicrophone()', 'background: #0000ff; color: #fff');
        let audioInterface = this.client.getMediaServices().getAudioInterface().getInputInterface();
        console.log(audioInterface)
        console.log(device)
        audioInterface.setSelectedDevice(device);
    }

    setCamera = (device) => {
        console.log('%c setCamera()', 'background: #0000ff; color: #fff');
        let videoInterface = this.client.getMediaServices().getVideoInterface();
        console.log(videoInterface)
        console.log(device)
        videoInterface.setSelectedDevice(device);
    }

    setSpeakers = (device) => {
        console.log('%c setSpeakers()', 'background: #0000ff; color: #fff');
        let speakersInterface = this.client.getMediaServices().getAudioInterface().getOutputInterface();
        console.log(speakersInterface)
        console.log(device)
        speakersInterface.setSelectedDevice(device);
    }

    addMicrophone = (label, deviceId) => {
        // console.log('%c addMicrophone()', 'background: #0000ff; color: #fff');
        this.micOptions[this.micOptions.length] = new Option(label, deviceId);
        this.EventEmitter.dispatch('state', { data: { micOptions: this.micOptions } })
    }

    addCamera = (label, deviceId) => {
        // console.log('%c addCamera()', 'background: #0000ff; color: #fff');
        this.cameraOptions[this.cameraOptions.length] = new Option(label, deviceId);
        this.EventEmitter.dispatch('state', { data: { cameraOptions: this.cameraOptions } })
    }

    addSpeaker = (label, deviceId) => {
        // console.log('%c addSpeaker()', 'background: #0000ff; color: #fff');
        this.speakersOptions[this.speakersOptions.length] = new Option(label, deviceId);
        this.EventEmitter.dispatch('state', { data: { speakersOptions: this.speakersOptions } })
    }

    selectMic = () => {
        console.log('%c selectMic()', 'background: #0000ff; color: #fff');
        let list1 = document.getElementById('micList');
        console.log("Selected index " + list1.selectedIndex);
        let mic = list1.options[list1.selectedIndex].value;
        console.log("Mic " + mic);
        if (list1.selectedIndex !== 0) {
            this.setMicrophone(this.micDevices[list1.selectedIndex]);
        }
    }

    selectCamera = () => {
        console.log('%c selectCamera()', 'background: #0000ff; color: #fff');
        let list1 = document.getElementById('cameraList');
        console.log("Selected index " + list1.selectedIndex);
        let camera = list1.options[list1.selectedIndex].value;
        console.log("Camera " + camera);
        if (list1.selectedIndex !== 0) {
            this.setCamera(this.cameraDevices[list1.selectedIndex]);
        }
    }

    selectSpeakers = () => {
        console.log('%c selectSpeakers()', 'background: #0000ff; color: #fff');
        let list1 = document.getElementById('speakersList');
        console.log("Selected index " + list1.selectedIndex);
        let speaker = list1.options[list1.selectedIndex].value;
        console.log("Speaker " + speaker);
        if (list1.selectedIndex !== 0) {
            this.setSpeakers(this.speakerDevices[list1.selectedIndex]);
        }
    }

    updateScreenshareSocketPresence = (state) => {
        console.log('%c updateScreenshareSocketPresence()', 'background: #0000ff; color: #fff');
        let presencePayload = {
            "category": "app.event.presence.party.online",
            "content":
            {
                "desktop": false,
                "idle": false,
                "mediaSession":
                {
                    "audio": !this.audioMuted,
                    "connected": true,
                    "phone": false,
                    "screenshare": state,
                    "selfMuted": false,
                    "video": !this.videoMuted
                },
                "offline": false,
                "role": "guest"
            },
            "topicId": this.spaceId
        };
        this.socketConnection.emit('SEND_PRESENCE_EVENT', presencePayload);
    }
    
    toggleScreenShare = (state) => {
        console.log('%c toggleScreenShare()', 'background: #0000ff; color: #fff');
        if (state) {
            console.log("Starting Screenshare");
            this.updateScreenshareSocketPresence(state);
            this.collaboration.getContentSharing().startScreenSharing()
            .catch(err => {
                console.log("Screensharing was interrupted: " + err)
                this.onScreenShare = false
                this.EventEmitter.dispatch('state', { data: { onScreenShare: this.onScreenShare }})
            })  
        } else {
            this.updateScreenshareSocketPresence(state);
            this.collaboration.getContentSharing().end();
            this.startLocalVideo2(this.localStream);
        }
    }

    startLocalVideo2 = (stream) => {
        console.log('%c startLocalVideo2()', 'background: #0000ff; color: #fff');
        console.log("Starting local video");
        var video = document.querySelector("#localVideoElement");
        video.srcObject = stream;
    }

    toggleRecording = (recordingState) => {
        console.log('%c toggleRecording()', 'background: #0000ff; color: #fff');
        if (recordingState) this.startRecordingFunction()
        else this.stopRecording()
    }

    stopRecording = () => {
        console.log(this.spaceId + '/meetings/' + this.meetingId + '/recordings/' + this.recordingId + '/stop')
        console.log('%c stopRecording()', 'background: #0000ff; color: #fff');
        axios({
            method: 'POST',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/meetings/' + this.meetingId + '/recordings/' + this.recordingId + '/stop',
            headers: {
                'Authorization': this.tokenType + " " + this.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
        }).then((res) => {
            this.recording = false
            this.EventEmitter.dispatch('state', { data: { recording: this.recording }})
        }).catch((err) => {
            console.log(`Stop meeting recording error: ${err}`);
        }).catch(err => this.EventEmitter.dispatch('error', { message: 'Could not stop recording', data: JSON.stringify(err, null, " ") }))
    }


    getActiveMeeting = () => {
        console.log('%c getActiveMeeting()', 'background: #0000ff; color: #fff');
        axios({
            method: 'GET',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/activemeeting',
            headers: {
                'Authorization': this.tokenType + " " + this.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
        }).then((res) => {
            console.log("Active meeting: ", res)
            this.meetingId = res.data["_id"];
            console.log("MEETING STARTED!")
            console.log(res)
            this.meetingData = res.data
            this.EventEmitter.dispatch('state', {
                data: { meetingData: this.meetingData },
            })
        }).catch((err) => {
            console.log(`Get Meeting ID Error: ${err}`);
        })
    }

    startRecordingFunction = () => {
        console.log('%c startRecordingFunction()', 'background: #0000ff; color: #fff');
        axios({
            method: 'POST',
            url: `https://${localStorage.getItem('spaces_apis')}.com/api/spaces/` + this.spaceId + '/meetings/' + this.meetingId + '/recordings',
            headers: {
                'Authorization': this.tokenType + " " + this.token,
                'Accept': 'application/json',
                'Content-type': 'application/json',
            },
        }).then(res => {
            this.recording = true
            this.EventEmitter.dispatch('state', { data: { recording: this.recording }})
        }).catch((err) => {
            console.log(`Recording error ${err}`);
            this.EventEmitter.dispatch('error', {
                message: 'Could not create a new chat message in a specified space', data: JSON.stringify(err, null, " ")
            })
        })
    }

    isConnected = (id) => {	
        return this.spaceMembers[id].content.mediaSession.connected === true	
    };	

    isConnecting = () => {	
        return this.call.getState() === 'initiating'	
    };	

    isCallStateConnected = () => {	
        return this.call.getState() === 'established'	
    }

    createMeetingInfoTXT = (meeting) => {
    	const {startTime, endTime, attendees } = meeting.content
        //TODO: Create follow the template below
        const fileObj = attendees.map(item => `${item.displayname} ${item.username} ${moment(item.joinTime).format('YYYY-MM-DD HH:mm:ss')+"\n"}`)
        console.log(fileObj)
        const file = `
Avaya Spaces Meeting 
        
Space name: 
Report generated on: ${moment(Date.now()).format('YYYY-MM-DD HH:mm:ss')}
Start Time: ${moment(startTime).format('YYYY-MM-DD HH:mm:ss')}
End Time: ${moment(endTime).format('YYYY-MM-DD HH:mm:ss')}
Participants:
        
Name, Email, Joining Time
${fileObj}
        `;
        var blob = new Blob([file], { type: 'text/plain;charset=utf-8' });
        saveFile(blob, 'meeting-info.txt');
      };

}
