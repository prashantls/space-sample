import React, { useEffect, useRef } from 'react';
import Message from '../components/Message'
import UserTyping from '../components/UserTyping'
import Post from '../components/Post'	
import Task from '../components/Task'	
import Meeting from '../components/Meeting'
import LiveMeeting from '../components/LiveMeeting';
import { toUTCDate } from '../utils'
import { sendMessage } from '../redux/actions/spaceActions'
import { connect } from 'react-redux'
import TextEditor from '../components/TextEditor'	


const mapStateToProps = state => {
    return {
        partyTyping: state.data.partyTyping,
        spaceContent: state.data.spaceContent,
        meetingData: state.data.meetingData,
    }
}

function Chat(props) {

    const contentRef = useRef();
    const { spaceContent, partyTyping, meetingData } = props
    const liveMeetingOngoing = meetingData?.content?.endTime === null

    useEffect(() => {	
        contentRef.current.scrollTop = contentRef.current.scrollHeight - contentRef.current.clientHeight	
    }, [spaceContent, partyTyping, meetingData])

    return (	
        <div className="chat-container">	
            <div className="log" ref={contentRef} >	
                {	
                    spaceContent.map((item, idx) => {	
                        if (item.category === 'chat') return <Message messageTxt={item.content.bodyText} avatarURL={item.sender.picture_url} createdOn={toUTCDate(item.created)} key={idx} sender={item.sender._id} />	
                        if (item.category === 'meeting') return <Meeting item={item} key={idx} />	
                        if (item.category === 'idea') return <Post post={item} key={idx} />	
                        if (item.category === 'task') return <Task task={item} key={idx} />	
                        else return null	
                    })	
                }	
                {liveMeetingOngoing && <LiveMeeting/> }
                {partyTyping && <UserTyping />}	
            </div>	
                <TextEditor />
        </div>	
    )	
}

export default connect(mapStateToProps, { sendMessage })(Chat)
