import './App.scss';
import { connect } from 'react-redux'
import { setErrorData, toggleErrorAlert, setZoom, toggleAuthModal, setAuthMethod, setEnvironment } from '../redux/actions/spaceActions'

import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import SpaceContent from './SpaceContent'
import DeviceSelectionModal from '../components/DeviceSelectionModal'
import MeetingControls from '../components/MeetingControls'
import AuthWall from '../components/AuthWall'
import ModalPostCreate from '../components/ModalPostCreate'
import ModalPostEdit from '../components/ModalPostEdit'
import ModalTaskCreate from '../components/ModalTaskCreate'
import ModalTaskEdit from '../components/ModalTaskEdit'
import ErrorAlert from '../components/ErrorAlert'
import SpaceMembersList from '../components/SpaceMembersList'
import StorageDashboard from '../components/StorageDashboard'
import DirectMessages from '../components/DirectMessages'
import UserSpaces from '../components/UserSpaces'
import CallPreviewModal from '../containers/CallPreviewModal'


const mapStateToProps = state => {
  return {
    me: state.data.me,
    peopleWithDM: state.data.peopleWithDM,
    contentSharingRendererZoom: state.data.contentSharingRendererZoom
  }
}
export class App extends Component {

  componentDidUpdate() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    if (urlParams.has('code') && this.props.me.length === 0) {
      window.SampleAvayaSpacesSDK.EventEmitter.subscribe('error', event => { this.props.setErrorData(event); this.props.toggleErrorAlert(true) })
      window.SampleAvayaSpacesSDK.EventEmitter.subscribe('state', event => { this.setState(event.data) })
    }
  }

  componentDidMount() {
    const QUERY_STRING = window.location.search;
    const URL_PARAMS = new URLSearchParams(QUERY_STRING);
    const CODE_IN_URL = URL_PARAMS.has('code')
    const AUTH_CODE_FLOW_CREDENTIALS_PRESENT = sessionStorage.getItem('client_id') && sessionStorage.getItem('client_secret') && sessionStorage.getItem('spaceId')

    if (CODE_IN_URL) {
      if (!AUTH_CODE_FLOW_CREDENTIALS_PRESENT) {
        window.location = 'http://localhost:3000'
      }
      if (AUTH_CODE_FLOW_CREDENTIALS_PRESENT) {
        sessionStorage.setItem('code', URL_PARAMS.get('code'));
        this.props.toggleAuthModal(false);
        this.props.setAuthMethod('authorizationCode')
      }
    }

    this.props.setEnvironment()
  }


  constructor(props) {
    super(props)
    this.videoGrid = document.getElementById('video-grid')
    this.myVideo = document.createElement('video')
    this.myVideo.muted = true;
  }

  displayErrorAlert = (event) => {
    this.setState({
      showErrorAlert: true,
      errorMessage: event.message,
      errorData: JSON.stringify(event.data, null, 2)
    })
  }

  render() {
    const { me, contentSharingRendererZoom, peopleWithDM } = this.props


    return (
      <div className="App">
        <span className="float-right">Logged in as: {me?.username ? <p>{me?.username}</p> : <Button variant="warning" onClick={() => this.setState({ showAuthModal: true })}>Authorize</Button>}</span>
        <ErrorAlert />
        <AuthWall />
        <CallPreviewModal />
        <ModalPostCreate />
        <ModalPostEdit />
        <ModalTaskCreate />
        <ModalTaskEdit />
        <DeviceSelectionModal />

        <h1>Sample Space App</h1>
        <div className='row'>
          <div className='col-6'>
            <h4>Stored application data:</h4>
            <StorageDashboard />
          </div>
          <div className='col-6'>
            <h4>Your camera view:</h4>
            <video autoPlay={true} id="localVideoElement" height="400" width="500" ref={this.textInput}></video>
            <MeetingControls />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-3">
            <h4>Space members:</h4>
            <SpaceMembersList />
            <br />
            {peopleWithDM && Object.keys(peopleWithDM).length ?
              <DirectMessages />
              : null}
          </div>
          <div className="col-6">
            <h4>Meeting video and chat:</h4>
            <video autoPlay={true} id="remoteVideoElement" height="350" width="600"></video>
            <SpaceContent />
          </div>
          <div className="col-3">
            <h4>User spaces:</h4>
            <UserSpaces />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className='col-12'>
            <h4>Screensharing frame:</h4>
            <p>Current size: {(contentSharingRendererZoom * 100)}%</p>
            <span onClick={() => setZoom(contentSharingRendererZoom + 0.25)}><Button>+</Button></span>
            <span onClick={() => setZoom(contentSharingRendererZoom - 0.25)}><Button>-</Button></span>
            <div id="screenReceiveFrame"></div>
          </div>
        </div>
      </div>
    )
  }
}



export default connect(mapStateToProps, { setErrorData, toggleErrorAlert, setZoom, toggleAuthModal, setAuthMethod, setEnvironment })(App)
