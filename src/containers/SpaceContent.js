import React, { useState, useEffect } from 'react';
import Chat from '../containers/Chat';
import Posts from '../containers/Posts';
import Tasks from '../containers/Tasks';
import Meetings from '../containers/Meetings';
import Tabs from 'react-bootstrap/Tabs'
import { Tab } from 'react-bootstrap';
import { connect } from 'react-redux'
import { getSpacePosts, getSpaceTasks, getSpaceMeetings } from '../redux/actions/spaceActions'


const mapStateToProps = state => {
    return {
        spaceTitle: state.data.spaceTitle,
    }
}

function SpaceContent(props) {
    const [eventKey, setEventKey] = useState('chatContainer');

    useEffect(() => {
        if (props.spaceTitle) {
            if (eventKey === "postsContainer") {
                props.getSpacePosts()
            }
            if (eventKey === 'tasksContainer') {
                props.getSpaceTasks()
            }
            if (eventKey === 'meetingsContainer') {
                props.getSpaceMeetings()
            }
        }
      },[eventKey, props]);

    return (
        <div>
            <h3>Joined space: {props.spaceTitle}</h3>
            <Tabs defaultActiveKey="chatContainer"
                transition={false}
                onSelect={function (k) { setEventKey(k) }}
            >
                <Tab eventKey="chatContainer" title="Chat">
                    <Chat />

                </Tab>
                <Tab eventKey="postsContainer" title="Posts">
                    <Posts />
                </Tab>
                <Tab eventKey="tasksContainer" title="Tasks">
                    <Tasks />
                </Tab>
                <Tab eventKey="meetingsContainer" title="Meetings">
                    <Meetings />

                </Tab>
            </Tabs>
        </div>
    )
}


export default connect(mapStateToProps, { getSpacePosts, getSpaceTasks, getSpaceMeetings })(SpaceContent)
