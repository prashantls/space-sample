import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { toggleDevicesModal } from '../redux/actions/spaceActions'
import DeviceSelectionBody from '../components/DeviceSelectionBody'

const mapStateToProps = state => {
    return {
        showDevicesModal: state.data.showDevicesModal,
    }
}

function DeviceSelectionModal(props) {
    const { showDevicesModal } = props

    useEffect(() => {
        if (showDevicesModal === true) {
            window.SampleAvayaSpacesSDK.displayMicDevices()
            window.SampleAvayaSpacesSDK.displayCameraDevices()
            window.SampleAvayaSpacesSDK.displaySpeakerDevices()
        }
    }, [showDevicesModal])

    return (
        <Modal show={showDevicesModal} onHide={() => props.toggleDevicesModal(!showDevicesModal)} animation={false} closeButton>
            <Modal.Header>
                <Modal.Title>Device settings</Modal.Title>
            </Modal.Header>
            
            <Modal.Body>
                <DeviceSelectionBody />
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={() => props.toggleDevicesModal(!showDevicesModal)}>Close</Button>
                <Button variant="primary" onClick={() => props.toggleDevicesModal(!showDevicesModal)}>Save changes</Button>
            </Modal.Footer>
        </Modal>


    )
}

export default connect(mapStateToProps, { toggleDevicesModal })(DeviceSelectionModal)

