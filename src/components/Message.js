import { connect } from 'react-redux'

const mapStateToProps = state => {
    return {
        me: state.data.me,
    }
}

function Message(props) {
  
  const createMarkup = () =>  {
    return {__html: messageTxt};
  }

  const { sender, avatarURL, messageTxt, createdOn, me } = props

  return (
    <div className={`chat-message ${sender === me._id && 'chat-message--right'} `}>
      <span className="chat-message__avatar-frame">
        <img src={avatarURL} alt="avatar" className="chat-message__avatar" />
      </span>
      <div>
        <p className="chat-message__text" dangerouslySetInnerHTML={createMarkup()}></p>
        <p className='createdOn'>{createdOn}</p>
      </div>
    </div>
  )
}

export default connect(mapStateToProps, {})(Message)


