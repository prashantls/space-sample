import React, { Component } from 'react'
import { debounce } from 'lodash-es'
import AsyncSelect from 'react-select/async';
import { connect } from 'react-redux'
import { getColleaguesAndDMList, joinDirectSpace } from '../redux/actions/spaceActions'

const mapStateToProps = state => {
    const { data } = state
    return {
        colleaguesAndDMList: data.colleaguesAndDMList,
    }
}
export class DMSearchBar extends Component {
    loadOptions = debounce((val, callback) => {
        const p = new Promise((resolve, reject) => {
            resolve(this.props.getColleaguesAndDMList(val))
        })
        p.then(() => {
            const newList = Object.keys(this.props.colleaguesAndDMList).map(idx => (
                {
                    "value": this.props.colleaguesAndDMList[idx]._id,
                    "label": `${this.props.colleaguesAndDMList[idx].displayname} <${this.props.colleaguesAndDMList[idx].username}>`
                }
            ))
            this.setState({ peopleList: newList }, () => {
                callback(this.state.peopleList)
            })
        })
    }, 1000)


    constructor(props) {
        super(props);
        this.state = {
            peopleList: [{ "value": '', 'label': '' }],
            query: ''
        }
    }

    render() {
        return (
            <div>
                <AsyncSelect
                    onInputChange={e => this.setState({ query: e })}
                    loadOptions={this.loadOptions}
                    onChange={obj => this.props.joinDirectSpace(obj.value)}
                    defaultOptions
                />

            </div>
        )
    }
}

export default connect(mapStateToProps, { getColleaguesAndDMList, joinDirectSpace })(DMSearchBar)
