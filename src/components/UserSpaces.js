import React from 'react'
import { connect } from 'react-redux'


const mapStateToProps = state => {
    return {
        mySpaces: state.data.mySpaces,
    }
}

function UserSpaces(props) {
    return (
        <div className='userSpaces'>
            {props.mySpaces && Object.keys(props.mySpaces).map(item => {
                return <p key={item} className='user-space' onClick={() => window.SampleAvayaSpacesSDK.joinSpace(props.mySpaces[item]._id)}>{props.mySpaces[item].title}</p>
            })}
        </div>
    )
}

export default connect(mapStateToProps)(UserSpaces)

