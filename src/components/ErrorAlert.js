import React from 'react'
import { Button, Alert, Accordion } from 'react-bootstrap';
import { connect } from 'react-redux'
import { toggleErrorAlert } from '../redux/actions/spaceActions'

const mapStateToProps = state => {
  return {
    showErrorAlert: state.data.showErrorAlert,
    errorMessage: state.data.errorMessage,
  }
}

function ErrorAlert(props) {
  return (
    props.showErrorAlert && (
      <Alert variant="danger" onClose={() => toggleErrorAlert(false)} dismissible>
        {props.errorMessage}
        <Accordion>
          <div>
            <Accordion.Toggle as={Button} eventKey="0">
              View error log ▼
      </Accordion.Toggle>
            <Button variant='danger' onClick={() => toggleErrorAlert(false)}>Close</Button>
            <Accordion.Collapse eventKey="0">
              <div><pre>{props.errorData}</pre>
              </div>
            </Accordion.Collapse>
          </div>
        </Accordion>
      </Alert>
    ))
}


export default connect(mapStateToProps, { toggleErrorAlert })(ErrorAlert)


