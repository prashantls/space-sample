import React from 'react'
import moment from 'moment'
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux'
import { toggleModalPostEdit } from '../redux/actions/spaceActions'



function Post(props) {
        return (
        <div className="post">
            <p >📰{props.post.content.bodyText}</p>
            <p >{props.post.sender.displayname}</p>
            <p className="createdOn">Created: {moment(props.post.created).format('YYYY-MM-DD HH:mm:ss')}</p>
            {props.editable && <Button className="m-0" onClick ={() => props.toggleModalPostEdit(true, props.post)}>Edit</Button>}
        </div>
    )
}

export default connect(null, { toggleModalPostEdit })(Post)